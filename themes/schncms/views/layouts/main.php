<?php $theme_url = $this->baseUrl . "/themes/schncms/views/layouts/"; ?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.8/admin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jul 2015 09:52:27 GMT -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo Yii::app()->name; ?> | Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/css/animate.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/css/style.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/css/style-responsive.min.css" rel="stylesheet"/>
    <link href="<?php echo $theme_url; ?>assets/css/theme/default.css" rel="stylesheet" id="theme"/>
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/ionRangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/password-indicator/css/password-indicator.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/jquery-tag-it/css/jquery.tagit.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo $theme_url; ?>assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
   <!-- <script src="<?php echo $theme_url; ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script> -->
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo $theme_url; ?>assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed in page-sidebar-minified">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">
            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand"><span class="navbar-logo"></span><?php echo Yii::app()->name; ?></a>
                <?php include_once dirname(__FILE__) . '/includes/header-menu.php'; ?>
            </div>
            <!-- end mobile sidebar expand / collapse button -->

            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form class="navbar-form full-width">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter keyword"/>
                            <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                        <i class="fa fa-bell-o"></i>
                        <span class="label">0</span>
                    </a>
                </li>
                <li class="dropdown navbar-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo $theme_url; ?>assets/img/user-13.jpg"
                             alt=""/>
                        <span class="hidden-xs"><?php echo Yii::app()->user->fullname; ?></span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li><a href="javascript:;">Thông tin tài khoản</a></li>
                        <li class="divider"></li>
                        <li><?php echo CHtml::link('Đăng xuất', array('site/logout')); ?></li>
                    </ul>
                </li>
            </ul>
            <!-- end header navigation right -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end #header -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo $this->pageTitle; ?>
            <small><?php echo $this->sub_title_name; ?>...</small>
        </h1>
        <!-- end page-header -->

        <?php echo $content; ?>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade"
       data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->
<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo $theme_url; ?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo $theme_url; ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/masked-input/masked-input.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/password-indicator/js/password-indicator.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js"></script>
	<script src="<?php echo $theme_url; ?>assets/plugins/jquery-tag-it/js/tag-it.min.js"></script>
    <script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-daterangepicker/moment.js"></script>
    <script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo $theme_url; ?>assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo $theme_url; ?>assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/js/form-plugins.demo.min.js"></script>
	<script src="<?php echo $theme_url; ?>assets/js/apps.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<script>
	$(document).ready(function() {
		App.init();
		FormPlugins.init();
        
        $(function () {

        var url = window.location.pathname, urlRegExp;
        // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
       // console.log('URL: ' + url);

        //var url_split = url.split('/');
        urlRegExp = new RegExp(url.replace(/\/$/, '') + '$');
        //console.log(url_split.length);
        /*  if(url_split.length >= 3){
         var urlAfter = url_split[1] + '.html';
         urlRegExp = new RegExp(urlAfter.replace(/\/$/,'') + '$');
         }else{
         if(url == '/'){
         var homeurl = '/trang-chu.html';
         urlRegExp = new RegExp(homeurl.replace(/\/$/,'') + '$');
         }else{
         urlRegExp = new RegExp(url.replace(/\/$/,'') + '$');
         }
         }*/
        // now grab every link from the navigation
       // console.log(urlRegExp);
        $('#main-menu a').each(function () {
            // and test its normalized href against the url pathname regexp
            if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
                $(this).parent().addClass('active');
                $(this).parent().parent().parent().addClass('active');
            }
        });
    });
	});
	</script>
</body>
</html>
