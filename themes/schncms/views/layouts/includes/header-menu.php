<?php $accountRoles = Yii::app()->user->roles;
$accountLevel0 = array("administrator");
$accountLevel1 = array("administrator", "admin");
$accountLevel2 = array("administrator", "admin", "manager");
$accountLevel3 = array("administrator", "admin", "manager", "content");
?>
<ul class="nav navbar-nav navbar-left" id="main-menu">
    <?php if(in_array($accountRoles, $accountLevel2)): ?>
        <li>
            <?php echo CHtml::link('Tổng quan', array('site/dashboard')); ?>
        </li>
    <?php endif;?>
    <?php if (in_array($accountRoles, $accountLevel3)): ?>
        <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <b class="caret pull-right"></b>
                <i class="fa fa-laptop"></i>
                <span>Sản phẩm</span>
            </a>
            <ul class="dropdown-menu animated fadeInLeft">
                <li>
                    <?php echo CHtml::link('Quản lý sản phẩm', array('books/admin')); ?>
                </li>
                <li>
                    <?php echo CHtml::link('Loại sản phẩm', array('categories/admin')); ?>
                </li>
                <li>
                    <?php echo CHtml::link('Nguồn nhập', array('sourcebooks/admin')); ?>
                </li>
            </ul>
        </li>
    <?php endif; ?>
    <?php if (in_array($accountRoles, $accountLevel3)): ?>
        <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <b class="caret pull-right"></b>
                <i class="fa fa-suitcase"></i>
                <span>Đơn hàng</span>
            </a>
            <ul class="dropdown-menu animated fadeInLeft">
                <li>
                    <?php echo CHtml::link('Khách hàng', array('customers/admin')); ?>
                </li>
                <li>
                    <?php echo CHtml::link('Quản lý đơn hàng', array('purchasebooks/admin')); ?>
                </li>

            </ul>
        </li>
    <?php endif; ?>
    <?php if (in_array($accountRoles, $accountLevel2)): ?>
         <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <b class="caret pull-right"></b>
                <i class="fa fa-suitcase"></i>
                <span>Báo cáo</span>
            </a>
            <ul class="dropdown-menu animated fadeInLeft">
                <li>
                    <?php echo CHtml::link('Thống kê lượng đơn hàng', '#'); ?>
                </li>
                <li>
                    <?php echo CHtml::link('Thống kê lượng sách bán', array('statistics/booksstistic')); ?>
                </li>

            </ul>
        </li>
        <?php endif; ?>
    <?php if (in_array($accountRoles, $accountLevel1)): ?>
        <li>
            <?php echo CHtml::link('Tài khoản', array('manageaccounts/admin')); ?>
        </li>
    <?php endif; ?>
</ul>