<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<div class="login-cover">
    <div class="login-cover-image"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt=""/>
    </div>
    <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <!-- begin brand -->
        <div class="login-header">
            <div class="brand">
                <span class="logo"></span> Color Admin
                <small>responsive bootstrap 3 admin template</small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'htmlOptions' => array(
                    'class' =>  'margin-bottom-0'
                ),
            )); ?>
                <div class="form-group m-b-20">
                    <!-- <input type="text" class="form-control input-lg" placeholder="Email Address"/> -->
                    <?php echo $form->textField($model,'username', array('class' => 'form-control input-lg', 'placeholder' => 'Tài khoản')); ?>
                </div>
                <div class="form-group m-b-20">
                 <!-- <input type="text" class="form-control input-lg" placeholder="Password"/> -->
                    <?php echo $form->passwordField($model,'password', array('class' => 'form-control input-lg', 'placeholder' => 'Mật khẩu')); ?>
                </div>
                <div class="checkbox m-b-20">
                    <label>
                        <input type="checkbox"/> Remember Me
                    </label>
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
                <div class="m-t-20">
                    Not a member yet? Click <a href="#">here</a> to register.
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <!-- end login -->

    <ul class="login-bg-list">
        <li class="active"><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-1.jpg" alt=""/></a>
        </li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-2.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-3.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-4.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-5.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-6.jpg" alt=""/></a></li>
    </ul>

</div>
<!-- end page container -->