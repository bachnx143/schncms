<?php

class ManageaccountsController extends Controller
{
    private $function;
    /**
     *
     *
     */
    public function init()
    {
        parent::init();
        if (!isset(Yii::app()->session['loggedin'])) {
            $this->redirect(array("site/login"));
        }

        $this->pageTitle = "Quản lý tài khoản";
        $this->function = new Functions();
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Manageaccounts;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manageaccounts']))
		{
			$model->attributes=$_POST['Manageaccounts'];
            $secretKey = $this->function->generatesalt(10);
            $accountPass = $_POST['Manageaccounts']['accountSecret'];

            $hashSecret = $this->function->hashPassword($accountPass, $secretKey);

            $model->accountSecret = $hashSecret;
            $model->accountSecretKey = $secretKey;

			if($model->save()){
                echo json_encode(array('status' => true, 'msg' => 'Tạo tài khoản thành công. '));
            }else{
                echo json_encode(array('status' => false, 'msg' => 'Tạo tài khoản thất bại. Vui lòng kiểm tra lại toàn bộ thông tin.'));
            }

		}else{
            $this->render('create',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manageaccounts']))
		{
			$model->attributes=$_POST['Manageaccounts'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Manageaccounts');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Manageaccounts('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manageaccounts']))
			$model->attributes=$_GET['Manageaccounts'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Manageaccounts the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Manageaccounts::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Manageaccounts $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manageaccounts-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionCheckAccount(){
        $requestedAccount  = $_REQUEST['Manageaccounts']['accountLogin'];
        $findAccount = Manageaccounts::model()->find("accountLogin = :account", array(":account" => $requestedAccount));
        if(!empty($findAccount))
            echo "false";
        else
            echo "true";
    }

    public function actionGetAccCode(){
        $function = new Functions;
        $accCode = $function->generateTableCode("account");
        echo $accCode;
    }
}
