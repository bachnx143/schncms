<?php

class PurchasebooksController extends Controller
{
    public $function;
    public function init()
    {
        parent::init();
        if (!isset(Yii::app()->session['loggedin'])) {
            $this->redirect(array("site/login"));
        }

        $this->pageTitle = "Danh sách đơn hàng";
        $this->function = new Functions;
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Purchasebooks;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Purchasebooks']))
		{
			$model->attributes=$_POST['Purchasebooks'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Purchasebooks']))
		{
			$model->attributes=$_POST['Purchasebooks'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{



		//$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Purchasebooks');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Purchasebooks('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Purchasebooks']))
			$model->attributes=$_GET['Purchasebooks'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Purchasebooks the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Purchasebooks::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Purchasebooks $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='purchasebooks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionAjaxBookName(){
        $request=trim($_GET['term']);
        if($request!=''){
            $model=Books::model()->findAll(array("condition"=>"bookName like '$request%'"));
            $data=array();
            foreach($model as $get){
                $data[]=$get->bookName;
            }
            $this->layout='empty';
            echo json_encode($data);
        }
    }

    public function actionAjaxGetBookCode(){
        $bookName = trim($_POST['bookname']);
        $model=Books::model()->find("bookName = :bookname", array(":bookname" => $bookName));

        if(!empty($model)){
            echo json_encode(array("bookcode" => $model->bookCode, "bookprice" => $model->exportPrice, 'existbook' => true));
        }else{
            $cateCode = "TL20150812084600639";
            $sourceCode = "NS20150810172525040";

            $newBook = new Books;
            $newBook->bookCode = $this->function->generateTableCode("books");
            $newBook->bookName = $bookName;
            $newBook->importedBy = Yii::app()->user->id;
            $newBook->importedByUser = Yii::app()->user->name;
            $newBook->importPrice = 0;
            $newBook->exportPrice = 0;
            $newBook->units = "cuốn";
            $newBook->quantity = 1;
            $newBook->publishingYear = date("Y", strtotime("last year"));
            $newBook->categoryId = trim($cateCode);
            $newBook->sourceId = trim($sourceCode);
            $newBook->importedAt = date("Y-m-d H:i:s");
            $newBook->importedBy = Yii::app()->user->id;
            $newBook->importedByUser = Yii::app()->user->name;
            $newBook->totalAmount = 0;

            $newBook->save(false);

            echo json_encode(array("bookcode" => $newBook->bookCode, "bookprice" => $newBook->exportPrice, 'existbook' => false));
        }


    }

    public function actionAjaxCusInfo(){
        $cusname = trim($_POST['cusname']);
        $model=Customers::model()->find("customerName = :customerName", array(":customerName" => $cusname));
        if(!empty($model))
            echo json_encode(array("cuscode" => $model->customerCode, "email" => $model->emailAddress, 'address' => $model->address, 'phone' => $model->phoneNumber, 'existcus' => true));
        else
            echo json_encode(array('existcus' => false));
    }

    public function actionAjaxCusName(){
        $request=trim($_GET['term']);
        if($request!=''){
            $model=Customers::model()->findAll(array("condition"=>"customerName like '$request%'"));
            $data=array();
            foreach($model as $get){
                $data[]=$get->customerName;
            }
            $this->layout='empty';
            echo json_encode($data);
        }
    }

    public function actionAjaxCusPhone(){
        $request=trim($_GET['term']);
        if($request!=''){
            $model=Customers::model()->findAll(array("condition"=>"phoneNumber like '$request%'"));
            $data=array();
            foreach($model as $get){
                $data[]=$get->phoneNumber;
            }
            $this->layout='empty';
            echo json_encode($data);
        }
    }

    public function actionAjaxCusInfoPhone(){
        $cusphone = trim($_POST['cusphone']);
        $model=Customers::model()->find("phoneNumber = :phonenumber", array(":phonenumber" => $cusphone));
        if(!empty($model))
            echo json_encode(array("cuscode" => $model->customerCode, "name" => $model->customerName, "email" => $model->emailAddress, 'address' => $model->address, 'phone' => $model->phoneNumber, 'existcus' => true));
        else
            echo json_encode(array('existcus' => false));
    }

    public function actionApplyOrder(){

        if(isset($_POST['data'])){
            $data = $_POST['data'];
            $orderCode = $this->function->generateTableCode("purchase");
            //{"cuscode":"KH20150811064538640","cusname":"Nguy\u1ec5n Xu\u00e2n B\u00e1ch","totalAmount":"40000","arrBooks":"SC20150811035550468:1,SC20150811035550468:3,"}
            $customerCode = $data['cuscode'];
            $customerName = $data['cusname'];
            $cusphone = $data['cusphone'];
            $cusemail = $data['cusemail'];
            $cusaddress = $data['cusaddress'];
            $shippingFee = $data['shippingFee'];
            $orderType = $data['orderType'];
            $orderStatus = $data['orderStatus'];
            $paymentMethod = $data['paymentMethod'];
            $orderNote = $data['orderNote'];

            $totalAmount = $data['totalAmount'];
            $arrBooks = $data['arrBooks'];
            $booSplit = explode(",", rtrim($arrBooks, ","));

            /** Extract Book Code and Quantity */
            foreach($booSplit as $itemBooks){
                $explodeItem = explode(":", $itemBooks);
                $bookCode = $explodeItem[0];
                $bookQuantity = $explodeItem[1];

                // update quantity after sell books
                $bookInfo = $this->subtractBooksUpdate($bookCode, $bookQuantity);
                $sourceCode = isset($bookInfo["sourceCode"]) ? $bookInfo["sourceCode"] : "";
                $cateCode = isset($bookInfo["cateId"]) ? $bookInfo["cateId"] : "";
                // add order list book
                $this->addBookListOrder($bookCode, $bookQuantity, $customerCode, $orderCode, $sourceCode, $cateCode);
            }

            // Add CustomerInfo
            $cusId = $this->addCustomer($customerCode, $customerName, $cusphone, $cusemail, $cusaddress);
            // Add purchase list
            $this->addPurchaseOrder($customerCode, $cusId, $totalAmount, $orderCode, $orderType, $shippingFee, $orderStatus, $paymentMethod, $orderNote);

            echo json_encode(array("status" => true));

        }
    }

    private function subtractBooksUpdate($bookCode, $bookQuantity){
        $findBook = Books::model()->find("bookCode = :bookcode", array(":bookcode" => $bookCode));
        $bookInfo = array();
        if(!empty($findBook)){
            $quantity = $findBook->quantity - $bookQuantity;

            $updateBook = array("quantity" => $quantity);
            Books::model()->updateByPk($findBook->id, $updateBook);

            $bookInfo = array("sourceCode" => $findBook->sourceId, "cateId" => $findBook->categoryId);
        }

        return $bookInfo;
    }

    private function addBookListOrder($bookCode, $bookQuantity, $customerCode, $purchasecode, $sourceId, $categoryId){
        $findListOrder = Orderlist::model()->find("customerCode = :cuscode and bookCode = :bookcode and purchaseCode = :purchasecode", array(':cuscode' => $customerCode, ':bookcode' => $bookCode, ":purchasecode" => $purchasecode));
        if(!empty($findListOrder)){
            $add_quantity = $bookQuantity + $findListOrder->quantity;
            $updateQuantity = array('quantity' => $add_quantity);
            Orderlist::model()->updateByPk($findListOrder->id, $updateQuantity);
        }else{
            $newOrderList = new Orderlist;
            $newOrderList->bookCode = $bookCode;
            $newOrderList->customerCode = $customerCode;
            $newOrderList->purchaseCode = $purchasecode;
            $newOrderList->quantity = $bookQuantity;
            $newOrderList->sourceCode = $sourceId;
            $newOrderList->categoryCode = $categoryId;

            $newOrderList->save(false);
        }

    }

    private function addCustomer($customerCode, $customername, $customerphone, $email, $addess){
        $returnCusId = "";
        $findCustomer = Customers::model()->find("customerCode = :cuscode ", array(":cuscode" => $customerCode));
        if(!empty($findCustomer)){
                $updateCus = array(
                    'customerName' => $customername,
                    'phoneNumber' => $customerphone,
                    'emailAddress' => $email,
                    'address' => $addess
                );
            Customers::model()->updateByPk($findCustomer->id, $updateCus);
            $returnCusCode = $findCustomer->id;

        }else{
            $newCustomer=new Customers;
            $newCustomer->customerCode = $customerCode;
            $newCustomer->customerName = $customername;
            $newCustomer->phoneNumber = $customerphone;
            $newCustomer->emailAddress = $email;
            $newCustomer->address = $addess;

            $newCustomer->save(false);

            $returnCusCode = $newCustomer->id;
        }
        return $returnCusCode;
    }

    private function addPurchaseOrder($cusCode, $cusId, $totalAmount, $orderCode, $orderType, $shippingFee, $orderStatus, $paymentMethod, $orderNote){

        $newOrder = new Purchasebooks;
        $newOrder->orderCode = $orderCode;
        $newOrder->customerCode = $cusCode;
        $newOrder->customerId = $cusId;
        $newOrder->totalAmount = $totalAmount;
        $newOrder->shippingFee = $shippingFee;
        $newOrder->orderType = $orderType;
        $newOrder->orderStatus = $orderStatus;
        $newOrder->paymentMethod = $paymentMethod;
        $newOrder->note = $orderNote;

        $newOrder->save(false);
    }

    private function findRelateOrderlist($orderCode){
        $findOnList = Orderlist::model()->findAll("purchaseCode = :purchasecode", array(":purchasecode" => $orderCode));
        if(!empty($findOnList)){
            foreach($findOnList as $itemBookCode){
                // Update quantity before delete book in list order
                $findBook = Books::model()->find("bookCode = :bookcode", array(":bookcode" => $itemBookCode->bookCode));
                if(!empty($findBook)){
                    $readdquantity = $findBook->quantity + $itemBookCode->quantity;
                    $updateBfDelete = array("quantity" => $readdquantity);
                    Books::model()->updateByPk($findBook->id, $updateBfDelete);
                }

                $itemorder = Orderlist::model()->findByPk($itemBookCode->id);

                $itemorder->delete();
            }
        }

    }

    public function actionUpdateBookPrice(){
        if($_POST["bookcode"]){
            $bookcode = $_POST["bookcode"];
            $bookprice = $_POST["bookprice"];

            $findBookCode = Books::model()->find("bookCode = :bookcode", array(":bookcode" => $bookcode));
            if(!empty($findBookCode)){
                $updateBookPrice = array("exportPrice" => $bookprice);
                $updated = Books::model()->updateByPk($findBookCode->id, $updateBookPrice);

                echo "result update is ".json_encode($updated);
            }
        }

    }

    public function actionAjaxupdate()
    {   $act = $_GET['act'];
        $autoIdAll = $_POST['autoId'];
        $del_fail = "";
        $del_success = "";
        $action_is = "";
        if(count($autoIdAll) > 0)
        {

            foreach($autoIdAll as $autoId)
            {
                $model=$this->loadModel($autoId);

                if($act=='doDelete'){
                    $action_is = 'del';
                    if($model->id == Yii::app()->user->id){
                        $del_fail .= $model->orderCode. ", ";
                        //echo CJSON::encode(array('status' => 'fail', 'message' => 'Không thể xoá tài khoản đang đăng nhập'));
                    }else{

                        $this->findRelateOrderlist($model->orderCode);
                        $model->delete();
                        $del_success .= $model->orderCode . ", ";
                        //echo CJSON::encode(array('status' => 'success', 'message' => 'Xoá thành công nhân '));
                    }
                }

            }

            switch($action_is){
                case 'del':
                    if(!empty($del_fail)){
                        echo CJSON::encode(array('msg' => 'Không thể xoá các đơn hàng:  ' . $del_fail . ', Xoá thành công các đơn hàng (' . $del_success . ')'));
                    }else{
                        echo CJSON::encode(array('msg' => 'Xoá thành công'));
                    }
                    break;
            }
        }

    }

    public function actionRepairPrice(){
        $model = Purchasebooks::model()->findAll();

        foreach($model as $item){
            if(strlen($item->totalAmount) >= 8){
                $repair = substr($item->totalAmount, 0, strlen($item->totalAmount) -3);
                //echo $item->totalAmount . " ==> " . $repair . "<br />";
                $updatePrice = Purchasebooks::model()->updateByPk($item->id, array('totalAmount' => $repair));
                echo $updatePrice. "<br />";
            }

        }
    }
    
    public function actionAddToOrder(){
        if(isset($_POST['submit'])){
            $COC_data = $_POST['data'];
            $COC_purchase = Customers::model()->find("customerCode = :code", array(":code" => $COC_data));
            if(!empty($COC_purchase)){
             echo json_encode(array("exist" => true, 
                "custcode" => $COC_purchase->customerCode, 
                "custname" => $COC_purchase->customerName, 
                "custemail" => $COC_purchase->emailAddress, 
                "custphone" => $COC_purchase->phoneNumber,
                "custaddress" => $COC_purchase->address ));   
            }else{
                echo json_encode(array("exist" => false));   
            }
        }
    }
}
