<?php

class BooksController extends Controller
{
    private $title_name;
    private $function;

    /**
     *
     *
     */
    public function init()
    {
        parent::init();
        if (!isset(Yii::app()->session['loggedin'])) {
            $this->redirect(array("site/login"));
        }

        $this->pageTitle = "Quản lý sách";
        $this->function = new Functions();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->sub_title_name = "Nhập sách mới";
        $model = new Books;
        $sourcemodel = new Sourcebooks;
        $categoriesmodel = new Categories;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Books'])) {
            $model->attributes = $_POST['Books'];
            $model->importedBy = Yii::app()->user->id;
            $model->importedByUser = Yii::app()->user->name;

            $checkCode = $this->checkExistBook("bookCode", $_POST['Books']['bookCode']);
            if ($checkCode['exist']) {

                if ($model->save(false)) {
                    echo json_encode(array('status' => true, 'msg' => '<strong>Thành công!</strong> Nhập sách mới thành công. '));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể nhập sách mới. Vui lòng kiểm tra lại thông tin. '));
                }

            } else {
                $findModel = $this->loadModel($checkCode['id']);
                $findModel->attributes = $_POST['Books'];

                if ($findModel->save()) {
                    echo json_encode(array('status' => true, 'msg' => '<strong>Thành công!</strong> Cập nhật sách thành công. '));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể cập nhật sách. Vui lòng kiểm tra lại thông tin. '));
                }
            }
        } else if (isset($_POST['Categories'])) {
            $categoriesmodel->attributes = $_POST['Categories'];
            $checkCateName = $this->checkExistCate($_POST['Categories']['categoryName']);
            if ($checkCateName) {
                if ($categoriesmodel->save()) {
                    $dropdownCate = $this->categoriesHtml();
                    echo json_encode(array('status' => true, 'dropdown' => $dropdownCate, 'msg' => '<strong>Thành công!</strong> Thêm thể loại thành công.'));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể thêm mới thể loại. Vui lòng kiểm tra lại thông tin.'));
                }
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Thể loại đã tồn tại. Vui lòng chọn tên khác'));
            }
        } else if (isset($_POST['Sourcebooks'])) {
            $checkSource = $this->checkExistSource($_POST['Sourcebooks']['sourceName']);
            if ($checkSource) {
                $sourcemodel->attributes = $_POST['Sourcebooks'];
                if ($sourcemodel->save()) {
                    $dropdownSource = $this->sourcebookDropdown();
                    echo json_encode(array('status' => true, 'dropdown' => $dropdownSource, 'msg' => '<strong>Thành công!</strong> Nhập thêm nguồn sách thành công. '));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể thêm mới nguồn sách. Vui lòng kiểm tra lại thông tin.'));
                }
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Nguồn nhập đã tồn tại. Vui lòng kiểm tra lại. '));
            }
        } else {
            $this->render('create', array(
                'model' => $model,
                'sourcemodel' => $sourcemodel,
                'categoriesmodel' => $categoriesmodel
            ));
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public
    function actionUpdate($id)
    {
        $this->sub_title_name = "Cập nhật thông tin sách";
        $model = $this->loadModel($id);

        $sourcemodel = new Sourcebooks;
        $categoriesmodel = new Categories;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Books'])) {
            $model->attributes = $_POST['Books'];
            $model->updatedBy = Yii::app()->user->usercode;
            $model->updatedAt = date("Y-m-d H:i:s");

            if ($model->save(false)) {
                echo json_encode(array('status' => true, 'msg' => '<strong>Thành công!</strong> Lưu thay đổi thành công. '));
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể lưu thay đổi. Vui lòng kiểm tra lại thông tin. '));
            }

        } else if (isset($_POST['Categories'])) {
            $categoriesmodel->attributes = $_POST['Categories'];
            $checkCateName = $this->checkExistCate($_POST['Categories']['categoryName']);
            if ($checkCateName) {
                if ($categoriesmodel->save()) {
                    $dropdownCate = $this->categoriesHtml();
                    echo json_encode(array('status' => true, 'dropdown' => $dropdownCate, 'msg' => '<strong>Thành công!</strong> Thêm thể loại thành công.'));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể thêm mới thể loại. Vui lòng kiểm tra lại thông tin.'));
                }
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Thể loại đã tồn tại. Vui lòng chọn tên khác'));
            }
        } else if (isset($_POST['Sourcebooks'])) {
            $checkSource = $this->checkExistSource($_POST['Sourcebooks']['sourceName']);
            if ($checkSource) {
                $sourcemodel->attributes = $_POST['Sourcebooks'];
                if ($sourcemodel->save()) {
                    $dropdownSource = $this->sourcebookDropdown();
                    echo json_encode(array('status' => true, 'dropdown' => $dropdownSource, 'msg' => '<strong>Thành công!</strong> Nhập thêm nguồn sách thành công. '));
                } else {
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể thêm mới nguồn sách. Vui lòng kiểm tra lại thông tin.'));
                }
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Nguồn nhập đã tồn tại. Vui lòng kiểm tra lại. '));
            }
        } else {
            $this->render('update', array(
                'model' => $model,
                'sourcemodel' => $sourcemodel,
                'categoriesmodel' => $categoriesmodel
            ));
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public
    function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public
    function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Books');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public
    function actionAdmin()
    {
        $model = new Books('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Books']))
            $model->attributes = $_GET['Books'];

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // would interfere with pager and repetitive page size change
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Books the loaded model
     * @throws CHttpException
     */
    public
    function loadModel($id)
    {
        $model = Books::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Books $model the model to be validated
     */
    protected
    function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'books-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    private
    function categoriesHtml()
    {
        $listCategories = CHtml::listData(Categories::model()->findAll(), "categoryCode", "categoryName");

        $cateDropdown = '';

        $cateDropdown .= '<select id="Books_categoryId" name="Books[categoryId]" class="form-control">';
        $cateDropdown .= '<option value="">Thể loại sách</option>';
        foreach ($listCategories as $item_index => $item_value) {
            $cateDropdown .= '<option value="' . $item_index . '">' . $item_value . '</option>';
        }
        $cateDropdown .= '</select>';

        return $cateDropdown;
    }

    private
    function checkExistCate($catename)
    {
        $isExistCate = true;
        $findmodel = Categories::model()->find('categoryName = :catename', array(':catename' => $catename));
        if (!empty($findmodel))
            $isExistCate = false;

        return $isExistCate;
    }

    private function checkExistSource($sourceName)
    {
        $isExistSource = true;
        $findSource = Sourcebooks::model()->find('sourceName = :sourcename', array(":sourcename" => $sourceName));

        if (!empty($findSource))
            $isExistSource = false;

        return $isExistSource;
    }

    private function sourcebookDropdown()
    {
        $listSource = CHtml::listData(Sourcebooks::model()->findAll(), 'sourceCode', 'sourceName');
        $dropdownSource = '';
        $dropdownSource .= '<select id="Books_sourceId" name="Books[sourceId]" class="form-control">';
        $dropdownSource .= '<option value=""> Chọn nguồn </option>';
        foreach ($listSource as $item_index => $item_name) {
            $dropdownSource .= '<option value="' . $item_index . '"> ' . $item_name . ' </option>';
        }
        $dropdownSource .= '</select>';

        return $dropdownSource;
    }

    private function checkExistBook($fieldName, $valueName)
    {
        $isExist = true;
        $bookId = 0;
        $findmodel = Books::model()->find("$fieldName = :name", array(":name" => $valueName));

        if (!empty($findmodel)) {
            $isExist = false;
            $bookId = $findmodel->id;
        }


        return array('exist' => $isExist, 'id' => $bookId);
    }

    public function actionAjaxBookcode()
    {
        $request = trim($_GET['term']);
        if ($request != '') {
            $model = Books::model()->findAll(array("condition" => "bookCode like '%$request%'"));
            $data = array();
            foreach ($model as $get) {
                $data[] = $get->bookCode;
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }

    public function actionAjaxBookName()
    {
        $request = trim($_GET['term']);
        if ($request != '') {
            $model = Books::model()->findAll(array("condition" => "bookName like '%$request%'"));
            $data = array();
            foreach ($model as $get) {
                $data[] = $get->bookName;
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }

    public function actionGenerateCode()
    {
        $time = "SC" . date("ymdhis");

        echo $time . " == " . strlen($time) . " (characters)";
    }

    public function actionImportFile()
    {
        header('Content-Type: text/html; charset=utf-8');
        set_time_limit(0);

        if (isset($_POST['submit_inport'])) {
            if (is_uploaded_file($_FILES['import_file']['tmp_name'])) {
                echo "<h1>" . "File " . $_FILES['import_file']['name'] . " uploaded successfully." . "</h1>";
                echo "<h2>Displaying contents:</h2>";
            }
            echo "<hr />";

            $srow = 1;
            $sheetname = $_POST["sheetname"];
            $inputFileType = 'Excel2007';

            spl_autoload_unregister(array('YiiBase', 'autoload'));

            Yii::import('application.extensions.PHPExcel.PHPExcel', true);

            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            /**  Advise the Reader of which WorkSheets we want to load  **/
            $objReader->setLoadSheetsOnly($sheetname);
            /**  Load $inputFileName to a PHPExcel Object  **/

            $objPHPExcel = $objReader->load($_FILES['import_file']['tmp_name']); //PHPExcel_IOFactory::load($file);
            $countInsert = 0;
            $countRow = 0;
            $countExist = 0;
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                echo "<br>The worksheet " . $worksheetTitle . " has ";
                echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
                echo ' and ' . $highestRow . ' row.<BR><BR>';
            }

            for ($row = $srow - 1; $row <= $highestRow - 2; ++$row) {
                $countRow++;

                $cell0_bookName = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                $cell1_units = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $cell2_partner = $worksheet->getCellByColumnAndRow(2, $row)->getOldCalculatedValue();
                $cell2_partner_notOldCalc = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $cell3_quantity = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $cell4_importPrice = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $cell5_exportPrice = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $cell6_categories = $worksheet->getCellByColumnAndRow(6, $row)->getValue();

                $cell2_final = (!empty($cell2_partner)) ? $cell2_partner : $cell2_partner_notOldCalc;
                $cell2_final = (!empty($cell0_bookName) && !empty($cell2_final)) ? $cell2_final : "Chưa rõ nguồn";

                /** Check categories is exist. If not create new and add to cache */
                if (!empty($cell6_categories)) {
                    $replace_cate = $this->splitName($cell6_categories);
                } else {
                    $replace_cate = "UnCategories";
                }

                /** */
                $cateCode = $this->checkCategoryCode(trim($replace_cate));
                if (!empty($cell0_bookName)) {
                    $sourceCode = $this->checkBookSource($cell2_final);
                }

              //  echo $cell6_categories . "-- " . $replace_cate . " - " . $cateCode . "<br />";

                /** Insert Book to Database */ //NXT-T8.2015
                 if(Yii::app()->user->name == 'bachnx'){
                    if (!empty($cell0_bookName)) {

                    $checkBookIsExist = Books::model()->find("bookName = :bookname", array(":bookname" => $cell0_bookName));
                    /** if(empty($checkBookIsExist)){

                        $newBook = new Books;
                        $newBook->bookCode = $this->function->generateTableCode("books");
                        $newBook->bookName = $cell0_bookName;
                        $newBook->importedBy = Yii::app()->user->id;
                        $newBook->importedByUser = Yii::app()->user->name;
                        $newBook->importPrice = !empty($cell4_importPrice) ? $cell4_importPrice . "000" : 0;
                        $newBook->exportPrice = !empty($cell5_exportPrice) ? $cell5_exportPrice . "000" : 0;
                        $newBook->units = $cell1_units;
                        $newBook->quantity = $cell3_quantity;
                        $newBook->publishingYear = date("Y", strtotime("last year"));
                        $newBook->categoryId = trim($cateCode);
                        $newBook->sourceId = trim($sourceCode);
                        $newBook->importedAt = date("Y-m-d H:i:s");
                        $newBook->importedBy = Yii::app()->user->id;
                        $newBook->importedByUser = Yii::app()->user->name;
                        $newBook->totalAmount = ($cell4_importPrice * $cell3_quantity);

                        $inserted = $newBook->save(false);
                        echo "INSERTED: " . $cell0_bookName . " -> " . json_encode($inserted) . "<br />";
                        $countInsert++;
                    }else{
                        $countExist++;
                    } */

                    if (!empty($checkBookIsExist)) {
                        $quantity = $checkBookIsExist->quantity + $cell3_quantity;
                        $totalAmount = ($cell4_importPrice * $quantity);

                        $update = array("quantity" => $quantity, "totalAmount" => $totalAmount);
                        $updated = Books::model()->updateByPk($checkBookIsExist->id, $update);
                        echo "UPDATED: " . $cell0_bookName . " -> " . json_encode($updated) . "<br />";
                        $countInsert++;
                    } else {
                        $newBook = new Books;
                        $newBook->bookCode = $this->function->generateTableCode("books");
                        $newBook->bookName = $cell0_bookName;
                        $newBook->importedBy = Yii::app()->user->id;
                        $newBook->importedByUser = Yii::app()->user->name;
                        $newBook->importPrice = !empty($cell4_importPrice) ? $cell4_importPrice . "000" : 0;
                        $newBook->exportPrice = !empty($cell5_exportPrice) ? $cell5_exportPrice . "000" : 0;
                        $newBook->units = $cell1_units;
                        $newBook->quantity = $cell3_quantity;
                        $newBook->publishingYear = date("Y", strtotime("last year"));
                        $newBook->categoryId = trim($cateCode);
                        $newBook->sourceId = trim($sourceCode);
                        $newBook->importedAt = date("Y-m-d H:i:s");
                        $newBook->importedBy = Yii::app()->user->id;
                        $newBook->importedByUser = Yii::app()->user->name;
                        $newBook->totalAmount = ($cell4_importPrice * $cell3_quantity);

                        $inserted = $newBook->save(false);
                        echo "INSERTED: " . $cell0_bookName . " -> " . json_encode($inserted) . "<br />";
                        $countExist++;
                    }
                }
                }else{
                    echo "Empty";
                }
            }

            echo "Inserted Row: " .$countInsert . " Of " . $countRow . " Rows <br />" ;
            echo "Exist Row: " .$countExist. " Of " . $countRow . " Rows <br />";

            spl_autoload_register(array('YiiBase', 'autoload'));

        } else {
            echo "Hello";
        }

    }

    private function checkCategoryCode($cateName)
    {
        $cacheTime = 120;
        $cacheCateId = $this->function->diacriticalMarks($cateName);
        $valCateCode = Yii::app()->cache->get($cacheCateId);

        if ($valCateCode)
            return $valCateCode;

        $findCate = Categories::model()->find("categoryName = :name", array(":name" => $cateName));
        $cateCode = "";
        if (!empty($findCate)) {
            $cateCode = $findCate->categoryCode;

        } else {
            $newCate = new Categories;
            $newCate->categoryCode = $this->function->generateTableCode("categories");
            $newCate->categoryName = $cateName;
            $newCate->description = $cacheCateId;
            if ($newCate->save())
                $cateCode = $newCate->categoryCode;
        }

        Yii::app()->cache->set($cacheCateId, $cateCode, $cacheTime);

        return $cateCode;
    }

    private function checkBookSource($sourceName)
    {
        $cacheTime = 120;
        $sourceBookCode = "";
        $sourceNameCache = $this->function->diacriticalMarks($sourceName);
        $getCacheVal = Yii::app()->cache->get($sourceNameCache);

        if ($getCacheVal)
            return $getCacheVal;

        $findBookSource = Sourcebooks::model()->find("sourceName = :name", array(":name" => $sourceName));

        if (!empty($findBookSource)) {
            $sourceBookCode = $findBookSource->sourceCode;
        } else {
            $newSource = new Sourcebooks;
            $newSource->sourceName = $sourceName;
            $newSource->sourceCode = $this->function->generateTableCode("sourcebook");

            if ($newSource->save())
                $sourceBookCode = $newSource->sourceCode;
        }

        Yii::app()->cache->set($sourceNameCache, $sourceBookCode, $cacheTime);

        return $sourceBookCode;
    }

    private function splitName($strName)
    {
        $replace1 = preg_replace("/SC[0-9\/]/", "", $strName);
        $replace2 = preg_replace("/SC [0-9\/]/", "", $replace1);
        $replace3 = trim(preg_replace("/[0-9\/]/", "", $replace2));
        $replace4 = str_replace("-", "", $replace3);
        return $replace4;
    }

    public function actionTest()
    {
        $function = new Functions();
        //  echo $function->generateTableCode("account");
    }

    public function actionAjaxupdate()
    {
        $act = $_GET['act'];
        $autoIdAll = $_POST['autoId'];
        $del_fail = "";
        $del_success = "";
        $action_is = "";
        if (count($autoIdAll) > 0) {
            foreach ($autoIdAll as $autoId) {
                $model = $this->loadModel($autoId);
                if ($act == 'doDelete') {
                    $action_is = 'del';
                    if ($model->id == Yii::app()->user->id) {
                        $del_fail .= $model->bookName . ", ";
                        //echo CJSON::encode(array('status' => 'fail', 'message' => 'Không thể xoá tài khoản đang đăng nhập'));
                    } else {
                        $model->delete();
                        $del_success .= $model->bookName . ", ";
                        //echo CJSON::encode(array('status' => 'success', 'message' => 'Xoá thành công nhân '));
                    }
                }

                /**  if($model->save())
                 * echo 'ok';
                 * else
                 * throw new Exception("Sorry",500); */

            }

            switch ($action_is) {
                case 'del':
                    if (!empty($del_fail)) {
                        echo CJSON::encode(array('msg' => 'Không thể xoá các cuốn:  ' . $del_fail . ', Xoá thành công các cuốn (' . $del_success . ')'));
                    } else {
                        echo CJSON::encode(array('msg' => 'Xoá thành công'));
                    }
                    break;
            }
        }

    }

    public function actionRepairPrice($typeprice)
    {
        $model = Books::model()->findAll();

        foreach ($model as $item) {
            if (strlen($item->$typeprice) >= 8) {
                $repairImport = substr($item->$typeprice, 0, strlen($item->$typeprice) - 3);
                //echo $item->importPrice . " ==> " . $repairImport . "<br />";
                $updated = Books::model()->updateByPk($item->id, array($typeprice => $repairImport));
                echo $updated . "<br />";
            }

        }
    }

    public function actionAjaxGetinFobook()
    {
        if (isset($_POST['bookname'])) {
            $bookName = $_POST['bookname'];
            $findBook = Books::model()->find("bookName = :bookname", array(':bookname' => $bookName));
            if (!empty($findBook)) {
                echo json_encode(array(
                    'existbook' => true,
                    'bookcode' => $findBook->bookCode,
                    'importprice' => $findBook->importPrice,
                    'exportprice' => $findBook->exportPrice,
                    'category' => $findBook->categoryId,
                    'source' => $findBook->sourceId,
                    'units' => $findBook->units,
                    'quantity' => $findBook->quantity,
                    'publish' => $findBook->publishingYear,
                    'totalamount' => $findBook->totalAmount,
                    'note' => $findBook->note
                ));
            }
        }

    }
    
    public function actionFormplugin(){
         $model = new Books('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Books']))
            $model->attributes = $_GET['Books'];

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // would interfere with pager and repetitive page size change
        }
        
        $this->render('formplugin', array('model' => $model));
    }
    
}
