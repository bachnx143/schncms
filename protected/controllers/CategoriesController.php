<?php

class CategoriesController extends Controller
{
    public function init()
    {
        parent::init();
        if (!isset(Yii::app()->session['loggedin'])) {
            $this->redirect(array("site/login"));
        }

        $this->pageTitle = "Danh sách thể loại";
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Categories;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
            $model->attributes = $_POST['Categories'];
            $checkCateName = Categories::model()->checkExistCate($_POST['Categories']['categoryName']);
            if ($checkCateName) {
                if ($model->save()) {
                    echo json_encode(array('status' => true, 'msg' => '<strong>Thành công!</strong> Thêm thể loại thành công.'));
                }else{
                    echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể thêm mới thể loại. Vui lòng kiểm tra lại thông tin.'));
                }
            } else {
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Thể loại đã tồn tại. Vui lòng chọn tên khác'));
            }

		}else{

            $this->render('create',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
            if ($model->save()) {
                echo json_encode(array('status' => true, 'msg' => '<strong>Thành công!</strong> Lưu thay đổi thành công.'));
            }else{
                echo json_encode(array('status' => false, 'msg' => '<strong>Thất bại!</strong> Không thể cập nhật thông tin. Vui lòng kiểm tra lại thông tin.'));
            }
		}else{
            $this->render('update',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Categories');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Categories('search');
        $createnewmodel=new Categories;

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Categories']))
            $model->attributes=$_GET['Categories'];

        $this->render('admin',array(
            'model'=>$model,
            'createnew' => $createnewmodel
        ));

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Categories the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Categories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Categories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionAjaxupdate()
    {   $act = $_GET['act'];
        $autoIdAll = $_POST['autoId'];
        $del_fail = "";
        $del_success = "";
        $action_is = "";
        if(count($autoIdAll) > 0)
        {
            foreach($autoIdAll as $autoId)
            {
                $model=$this->loadModel($autoId);
                if($act=='doDelete'){
                    $action_is = 'del';
                    if($model->id == Yii::app()->user->id){
                        $del_fail .= $model->categoryName. ", ";
                        //echo CJSON::encode(array('status' => 'fail', 'message' => 'Không thể xoá tài khoản đang đăng nhập'));
                    }else{
                        $model->delete();
                        $del_success .= $model->categoryName . ", ";
                        //echo CJSON::encode(array('status' => 'success', 'message' => 'Xoá thành công nhân '));
                    }
                }

                /**  if($model->save())
                echo 'ok';
                else
                throw new Exception("Sorry",500); */

            }

            switch($action_is){
                case 'del':
                    if(!empty($del_fail)){
                        echo CJSON::encode(array('msg' => 'Không thể xoá các thể loại:  ' . $del_fail . ', Xoá thành công các thể loại (' . $del_success . ')'));
                    }else{
                        echo CJSON::encode(array('msg' => 'Xoá thành công'));
                    }
                    break;
            }
        }

    }
}
