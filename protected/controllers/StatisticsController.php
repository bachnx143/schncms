<?php 

class StatisticsController extends Controller
{
    private $title_name;
    private $function;

    /**
     *
     *
     */
    public function init()
    {
        parent::init();
        if (!isset(Yii::app()->session['loggedin'])) {
            $this->redirect(array("site/login"));
        }

        $this->pageTitle = "Thống kê";
        $this->function = new Functions();
    }
    
    public function actionBooksStistic(){
        $this->pageTitle = "Thống kê sản lượng sách";
        
        $bookdetail = new Orderlist("getdetailbooks");
        $bookdetail->unsetAttributes();
         if (isset($_GET['Orderlist']))
            $bookdetail->attributes = $_GET['Orderlist'];
         
         if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // would interfere with pager and repetitive page size change
        }
            
        $this->render('bookstatistic', 
                        array(
                            'booksmodel' => $bookdetail));
    }    
}