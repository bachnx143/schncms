<?php

class SiteController extends Controller
{

	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('*'),
				'actions'=>array('login','addadminaccount'),
			),
			array('allow',
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		 $this->pageTitle = "Tổng quan";

        $this->redirect(array('site/dashboard'));
	}

    public function actionDashboard(){
        $this->pageTitle = "Tổng quan";
        $OrderChartData = Purchasebooks::model()->getLastSeven();

        $this->render('dashboard', array(
            'dataorder' => $OrderChartData));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->pageTitle = "Đăng nhập";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
               $this->addLastLogin(Yii::app()->user->name);
                $accountLevel0 = array("administrator");
                $accountLevel1 = array("administrator", "admin");
                $accountLevel2 = array("administrator", "admin", "manager");
                $accountLevel3 = array("administrator", "admin", "manager", "content");
                $roles = Yii::app()->user->roles;
                if(in_array($roles, $accountLevel1) || in_array($roles, $accountLevel3)){
                    $this->redirect(array('site/dashboard'));
                }else{
                    $this->redirect(array('books/admin'));
                }
            }
				 //Yii::app()->user->returnUrl

			//$users = Manageaccounts::model()->find('accountLogin = :uname', array(':uname' => $model->username));

			//echo $users->accountLogin; die;

		}
		// display the login form
		$this->renderPartial('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionAddAdminAccount(){
		$function = new Functions;
//		$lstRoles = CHtml::listData(Roles::model()->findAll(),"id", "roleName");
//
//		echo '<form action="" method="post">';
//			echo '<select name="user_role">';
//				echo '<option value=""> -- Choose Role for user -- </option>';
//					foreach($lstRoles as $Code => $Name){
//						echo '<option value="'.$Code.'">'.$Name.'</option>';
//					}
//			echo '</select>';
//			echo '<input type="submit" value="Submit" name="submit"/>';
//		echo '</form>';
//
//		if(isset($_POST['submit'])){
//			$accountCode = $function->generateTableCode("sysaccount");
//			$secretKey = $function->generatesalt(10);
//			$modelRole = Roles::model()->findByPk($_POST['user_role']);
//
//			$admin = new Manageaccounts;
//
//			$admin->accountName = "Sách cũ Hà Nội";
//			$admin->accountCode = $accountCode;
//			$admin->roleCode = $modelRole->roleCode;
//			$admin->accountEmail = "schn@gmail.com";
//			$admin->accountLogin = "schnadmin";
//			$admin->accountSecret = $function->hashPassword("admin!$", $secretKey);
//			$admin->accountSecretKey = $secretKey;
//			$admin->phoneNumber = "";
//
//			//echo json_encode($admin->save(false));
//		}
       // echo $function->generateTableCode("account");
//        $role_name = array('administrator', 'admin', 'manager', 'content');
//        foreach($role_name as $itemrole){
//            $newRoles = new  Roles;
//            $newRoles->roleCode = $function->generateTableCode("roles");
//            $newRoles->roleName = $itemrole;
//           // $newRoles->save();
//        }
	}

    private function addLastLogin($accountName){
        $findAccount = Manageaccounts::model()->find("accountLogin = :account", array(":account" => $accountName));
        $updated_login = "";
        if(!empty($findAccount)){
            $time = date("Y-m-d H:i:s");
            $iplogin = $this->getRealIpAddr();

            $update_lognin = array('lastLoginAt' => $time, 'lastLoginIn' => $iplogin);
            $updated_login = Manageaccounts::model()->updateByPk($findAccount->id, $update_lognin);
        }

        return $updated_login;
    }

    /**
     * @function getRealIpAddr
     *
     * @access public
     * Return Ip Address
     */
    private function getRealIpAddr()
    {
        $ip = '0.0.0.0';
        if (isset($_SERVER['HTTP_CLIENT_IP']) || isset($_SERVER['HTTP_CLIENT_IP']) || isset($_SERVER['HTTP_X_FORWARDED_FOR']) || isset($_SERVER['REMOTE_ADDR'])) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
            {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        }
        return $ip;
    }
}