<?php
/* @var $this CategoriesController */
/* @var $data Categories */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoryCode')); ?>:</b>
	<?php echo CHtml::encode($data->categoryCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoryName')); ?>:</b>
	<?php echo CHtml::encode($data->categoryName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>