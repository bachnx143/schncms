<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
?>
<script>
    function reloadGrid() {
        $.fn.yiiGridView.update('categories-grid');
    }
</script>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning" href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger" href="javascript:;"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $this->pageTitle; ?></h4>
            </div>
            <div class="panel-body" style="display: block;" >
                <?php echo CHtml::link('Thêm thể loại', '#', array('class' => 'btn btn-primary btn-addcate', 'onclick' => '$("#add-categories").dialog("open"); return false;')); ?>
                <?php
                $form=$this->beginWidget('CActiveForm', array(
                    'enableAjaxValidation'=>true,
                ));
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'categories-grid',
                    'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                    'summaryText' => 'Hiển thị danh sách từ {start}-{end} trong tổng số {count} kết quả',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'columns'=>array(
                        array(
                            'id'=>'autoId',
                            'class'=>'CCheckBoxColumn',
                            'selectableRows' => '50',
                        ),
                        'categoryCode',
                        'categoryName',
                        'description',
                        array(
                            'class'=>'CButtonColumn',
                            'template' => '{update}',
                           /* 'buttons' => array(
                                'update' => array(
                                    'url'=>'$this->grid->controller->createUrl("update", array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                                    'click'=>'function(){$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',)
                            ) */
                        ),
                    ),
                ));

                echo CHtml::ajaxSubmitButton('Xoá thể loại',
                    array('categories/ajaxupdate','act'=>'doDelete'),
                    array('success'=>'function(data){
                                obj = JSON.parse(data);
                                    alert(obj.msg);
                                    location.reload();
                                    //reloadGrid
                            }'),
                    array('class' => 'btn-ajax btn btn-warning',"onclick"=>"confirm('Bạn muốn xoá các thể loại được chọn?')"));

                $this->endWidget();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
/** Start Add Source Books Widget **/
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'add-categories',
    'options' => array(
        'title' => 'Thêm mới thể loại',
        'autoOpen' => false,
        'width' => 800,
    ),
));
$this->renderPartial('_form', array('model' => $createnew));
$this->endWidget('zii.widgets.jui.CJuiDialog');
/** End Add Source Books Widget **/
?>

<?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
/** $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'cru-dialog',
    'options'=>array(
        'title'=>'Detail view',
        'autoOpen'=>false,
        'modal'=>false,
        'width'=>800,
    ),
)); */
?>
<!-- <iframe id="cru-frame" width="100%" height="100%"></iframe> -->
<?php

//$this->endWidget();
//--------------------- end new code --------------------------
?>

