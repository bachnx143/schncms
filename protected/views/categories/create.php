<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);
?>
<div class="row">
    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
