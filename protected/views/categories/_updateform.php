<?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'categories-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal'
        )
    )); ?>
    <div id="add-cate-alert" class="alert alert-warning fade in m-b-15" style="display: none;">
        <span class="alt-content">
            <strong>Warning!</strong>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </span>
        <span data-dismiss="alert" class="close">×</span>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'categoryCode', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->textField($model, 'categoryCode', array('class' => 'form-control cates-code', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model, 'categoryCode'); ?>
        </div>
        <div class="col-md-3">
            <?php echo CHtml::link('Lấy mã mới', '#', array('class' => 'btn btn-default btn-renew-code')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'categoryName', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'categoryName', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'categoryName'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textArea($model, 'description', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label"></label>

        <div class="col-md-9">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo thể loại mới' : 'Lưu thay đổi', array('class' => 'btn btn-success')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('createCate', "

    $('.btn-addcate').click(function(){
        var categories_time = 'cates'+Math.round(+new Date()/1000);
        $('.cates-code').val(categories_time);
    });

	$('.btn-renew-code').click(function(){
	    var newcode = 'cates'+Math.round(+new Date()/1000);
	    $('.cates-code').val(newcode);
	});

	/**  Validate categories */
	$('#categories-form').validate({
                onfocusout: true,
                onkeyup: true,
                onclick: true,
                focusInvalid: true,
                debug:true,
                rules:{
                    'Categories[categoryName]': 'required'
                },
                messages:{
                    'Categories[categoryName]': 'Chưa có tên thể loại. Hãy nhập tên cho thể loại. '
                },
                submitHandler: function(){
                    var formData = new FormData($('#categories-form')[0]);
                    var action = $('#categories-form').attr('action')

                       $.ajax({
                            url: action,
                            type: 'POST',
                            data: formData,
                            async: false,
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                console.log(obj.msg);
                                 if(obj.status){
                                    $('.alt-content').html(obj.msg);
                                    $('#add-cate-alert').removeAttr('style');
                                    $('#add-cate-alert').removeClass('alert-warning');
                                    $('#add-cate-alert').addClass('alert-success');

                                }else{
                                    $('.alt-content').html(obj.msg);
                                    $('#add-cate-alert').removeClass('alert-success');
                                    $('#add-cate-alert').addClass('alert-warning');
                                    $('#add-cate-alert').removeAttr('style');
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                }
            });


");
?>