<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"></h4>
            </div>
            <div class="panel-body" style="display: block;">
                <?php $this->widget('zii.widgets.CDetailView', array(
                	'data'=>$model,
                	'attributes'=>array(
                		'customerCode',
                		'customerName',
                		'phoneNumber',
                		'emailAddress',
                		'address',
                		'note',
                	),
                )); ?>
            </div>
        </div>
    </div>
</div>
