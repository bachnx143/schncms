<?php
/* @var $this CustomersController */
/* @var $model Customers */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#customers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-md-12 ui-sortable">
    <div class="panel panel-inverse">
        <div class="panel-heading">

        </div>
        <div class="panel-body" style="display: block">
            <?php $this->renderPartial('_search', array(
                'model' => $model,
            )); ?>
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'customers-grid',
                'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                'summaryText' => 'Hiển thị danh sách từ {start}-{end} trong tổng số {count} kết quả',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    'customerCode',
                    'customerName',
                    'phoneNumber',
                    'emailAddress',
                    'address',
                    /*
                    'note',
                    */
                    array(
                        'class'=>'CButtonColumn',
                        'template' => '{view}'
                    ),
                ),
            )); ?>
        </div>
    </div>
</div>

