<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array(
        'class' => 'form-inline'
    )
)); ?>

	<div class="form-group">
		<?php echo $form->textField($model,'customerCode',array('class' => 'form-control', 'placeholder' => 'Mã khách hàng')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'customerName',array('class' => 'form-control', 'placeholder' => 'Tên khách hàng')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'phoneNumber',array('class' => 'form-control', 'placeholder' => 'Số điện thoại')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'emailAddress',array('class' => 'form-control', 'placeholder' => 'Địa chỉ E-mail')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'address',array('class' => 'form-control', 'placeholder' => 'Địa chỉ')); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Tìm khách hàng', array('class' => 'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
