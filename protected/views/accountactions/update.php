<?php
/* @var $this AccountactionsController */
/* @var $model Accountactions */

$this->breadcrumbs=array(
	'Accountactions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Accountactions', 'url'=>array('index')),
	array('label'=>'Create Accountactions', 'url'=>array('create')),
	array('label'=>'View Accountactions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Accountactions', 'url'=>array('admin')),
);
?>

<h1>Update Accountactions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>