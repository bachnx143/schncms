<?php
/* @var $this AccountactionsController */
/* @var $model Accountactions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountCode'); ?>
		<?php echo $form->textField($model,'accountCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actionAt'); ?>
		<?php echo $form->textField($model,'actionAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actionIp'); ?>
		<?php echo $form->textField($model,'actionIp',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actionDescription'); ?>
		<?php echo $form->textField($model,'actionDescription',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->