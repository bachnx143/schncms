<?php
/* @var $this AccountactionsController */
/* @var $model Accountactions */

$this->breadcrumbs=array(
	'Accountactions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Accountactions', 'url'=>array('index')),
	array('label'=>'Manage Accountactions', 'url'=>array('admin')),
);
?>

<h1>Create Accountactions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>