<?php
/* @var $this AccountactionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accountactions',
);

$this->menu=array(
	array('label'=>'Create Accountactions', 'url'=>array('create')),
	array('label'=>'Manage Accountactions', 'url'=>array('admin')),
);
?>

<h1>Accountactions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
