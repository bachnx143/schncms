<?php
/* @var $this AccountactionsController */
/* @var $model Accountactions */

$this->breadcrumbs=array(
	'Accountactions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Accountactions', 'url'=>array('index')),
	array('label'=>'Create Accountactions', 'url'=>array('create')),
	array('label'=>'Update Accountactions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Accountactions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Accountactions', 'url'=>array('admin')),
);
?>

<h1>View Accountactions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'accountCode',
		'actionAt',
		'actionIp',
		'actionDescription',
	),
)); ?>
