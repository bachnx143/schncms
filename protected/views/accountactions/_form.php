<?php
/* @var $this AccountactionsController */
/* @var $model Accountactions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'accountactions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accountCode'); ?>
		<?php echo $form->textField($model,'accountCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'accountCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actionAt'); ?>
		<?php echo $form->textField($model,'actionAt'); ?>
		<?php echo $form->error($model,'actionAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actionIp'); ?>
		<?php echo $form->textField($model,'actionIp',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'actionIp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actionDescription'); ?>
		<?php echo $form->textField($model,'actionDescription',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'actionDescription'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->