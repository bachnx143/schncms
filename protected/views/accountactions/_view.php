<?php
/* @var $this AccountactionsController */
/* @var $data Accountactions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountCode')); ?>:</b>
	<?php echo CHtml::encode($data->accountCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actionAt')); ?>:</b>
	<?php echo CHtml::encode($data->actionAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actionIp')); ?>:</b>
	<?php echo CHtml::encode($data->actionIp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actionDescription')); ?>:</b>
	<?php echo CHtml::encode($data->actionDescription); ?>
	<br />


</div>