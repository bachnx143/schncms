<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array(
        'class' => 'form-inline form-bordered'
    )
)); ?>
    <div class="form-group">
			<div class="input-group"> <!-- id="default-daterange" -->
                    <?php echo $form->textField($model, 'daterange', array('class' => 'form-control', 'placeholder' => 'Tìm từ ngày đến ngày'));?>
				    <span class="input-group-btn">
						<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
		</div>
        <div class="form-group">
            <?php $lstcate = Orderlist::model()->getFieldData(); ?>
            <?php echo $form->dropDownList($model, 'categoryCode', $lstcate, array('class' => 'form-control', 'empty' => 'Chọn thể loại')); ?>
        </div>
        
        <div class="form-group">
            <?php $lstsource = Orderlist::model()->getSourceData(); ?>
            <?php echo $form->dropDownList($model, 'sourceCode', $lstsource, array('class' => 'form-control', 'empty' => 'Chọn nguồn nhập')); ?>
        </div>
        
        <div class="form-group">
		<?php echo CHtml::submitButton('Tìm sách', array('class' => 'btn btn-sm btn-primary m-r-5')); ?>
	</div>
<?php $this->endWidget(); ?>

<?php 
Yii::app()->clientScript->registerScript('searchstatistic', "
    $(function() {
        $('input#Orderlist_daterange').daterangepicker();
    });
");
?>
