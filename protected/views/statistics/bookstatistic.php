<?php $pageSize = Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-body" style="display: block;">
                <?php $this->renderPartial('includes/_searchstatistic', array(
                    'model' => $booksmodel,
                )); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Thống kê sản lượng sách </h4>
            </div>
            <div class="panel-body" style="display: block">
                <?php
                
                $this->widget('zii.widgets.grid.CGridView', array(
                	'id'=>'statisticbook-grid',
                    'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                	'dataProvider'=>$booksmodel->getdetailbooks(),
                	'filter'=>$booksmodel,
                	'columns'=>array(
                        'bookCode',
                        'bookName',
                        'sourceName',
                        'importPrice',
                        'totalQ',
                        'exportPrice',
                        'totalAmount',
                        array(
                                'class' => 'CButtonColumn',
                                'template' => '',
                                'header' => CHtml::dropDownList('pageSize', $pageSize, array(20 => 20, 50 => 50, 100 => 100), array(
                                        // change 'user-grid' to the actual id of your grid!!
                                        'onchange' => "$.fn.yiiGridView.update('books-grid',{ data:{pageSize: $(this).val() }})",
                                    )),
                            ),
                	),
                )); ?>
            </div>
        </div>
    </div>
    
</div>