<?php
/* @var $this PurchasebooksController */
/* @var $model Purchasebooks */

$this->breadcrumbs = array(
    'Purchasebooks' => array('index'),
    'Manage',
);
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function(){
        $('#purchasebooks-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

    <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search', array(
            'model' => $model,
        )); ?>
    </div><!-- search-form -->
    <div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default"
                       href="javascript:;"><i class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger"
                       href="javascript:;"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Nhập đơn hàng</h4>
            </div>
            <div class="panel-body" style="display: block;">
                <div id="add-purchase-alert" class="alert alert-warning fade in m-b-15" style="display: none;">
                    <span class="alt-content">
                        <strong>Warning!</strong>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </span>
                    <span data-dismiss="alert" class="close">×</span>
                </div>
                <form action="" method="" class="form-inline">
                    <div class="col-md-12 form-order">
                        <div class="form-group m-r-10">
                            <input type="text" name="customerCode" class="form-control customerCode" readonly="readonly"
                                   placeholder="Mã khách hàng"/>
                        </div>
                        <div class="form-group m-r-10">
                            <?php
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'name' => 'customerName',
                                // additional javascript options for the autocomplete plugin
                                'source' => $this->createUrl("purchasebooks/ajaxcusname"),
                                'options' => array(
                                    'minLength' => '4',
                                    'select' => "js:function(event, ui) {
                                        $.post('" . $this->createUrl("purchasebooks/ajaxcusinfo") . "',{cusname: ui.item.value}, function(returncus){
                                            var objcus = jQuery.parseJSON( returncus );
                                            if(objcus.existcus){
                                                $('.customerCode').val(objcus.cuscode);
                                                $('.customePhone').val(objcus.phone);
                                                $('.customerEmail').val(objcus.email);
                                                $('.customerAddress').val(objcus.address);
                                            }

                                        });
                                      }",
                                ),
                                'htmlOptions' => array(
                                    'class' => 'form-control customerName',
                                    'placeholder' => 'Tên khách hàng'
                                ),
                            ));
                            ?>
                        </div>
                        <div class="form-group m-r-10">
                            <!-- <input type="text" name="customePhone" class="form-control customePhone"
                                   placeholder="Số điện thoại"/> -->
                            <?php
                                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                    'name' => 'customePhone',
                                    // additional javascript options for the autocomplete plugin
                                    'source' => $this->createUrl("purchasebooks/ajaxcusphone"),
                                    'options' => array(
                                        'minLength' => '4',
                                        'select' => "js:function(event, ui) {
                                            $.post('" . $this->createUrl("purchasebooks/ajaxcusinfophone") . "',{cusphone: ui.item.value}, function(returncusphone){
                                                var objcusphone = jQuery.parseJSON( returncusphone );
                                                if(objcusphone.existcus){
                                                    $('.customerName').val(objcusphone.name);
                                                    $('.customerCode').val(objcusphone.cuscode);
                                                    $('.customePhone').val(objcusphone.phone);
                                                    $('.customerEmail').val(objcusphone.email);
                                                    $('.customerAddress').val(objcusphone.address);
                                                }

                                            });
                                          }",
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control customePhone',
                                        'placeholder' => 'Số điện thoại'
                                    ),
                                ));
                            ?>
                        </div>
                        <div class="form-group m-r-10">
                            <input type="text" name="customerEmail" class="form-control customerEmail"
                                   placeholder="Email khách hàng"/>
                        </div>
                        <div class="form-group m-r-35">
                            <input type="text" name="customerAddess" class="form-control customerAddress"
                                   placeholder="Địa chỉ"/>
                        </div>
                    </div>
                    <div class="col-md-12 form-order">
                        <div class="form-group m-r-10">
                            <input type="text" name="bookCode" class="form-control bookCode" readonly="readonly"
                                   placeholder="Mã sách"/>
                        </div>
                        <div class="form-group m-r-10">
                            <?php //echo $form->textField($model,'bookName',array('class' => 'form-control', 'placeholder' => 'Tên sách'));
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                'name' => 'bookName',
                                // additional javascript options for the autocomplete plugin
                                'source' => $this->createUrl("purchasebooks/ajaxbookname"),
                                'options' => array(
                                    'minLength' => '4',
                                    'select' => "js:function(event, ui) {
                                        $.post('" . $this->createUrl("purchasebooks/ajaxgetbookcode") . "',{bookname: ui.item.value}, function(returncode){
                                            //console.log(returncode);
                                            var obj = jQuery.parseJSON( returncode );
                                            $('.bookCode').val(obj.bookcode);
                                            if(obj.bookprice == 0){
                                                $('.bookPrice').show();
                                            }else{
                                                $('.bookPrice').val(obj.bookprice);
                                            }

                                            $('.bookQuantity').val('1');
                                        });

                                      }",
                                ),
                                'htmlOptions' => array(
                                    'class' => 'form-control search-bookname',
                                    'placeholder' => 'Tên sách'
                                ),
                            ));
                            ?>
                        </div>
                        <div class="form-group m-r-10">
                            <input type="text" name="bookQuantity" value="" class="bookQuantity form-control"
                                   placeholder="Số lượng"/>
                        </div>
                        <div class="form-group m-r-10">
                            <input type="text" name="bookPrice" value="" class="bookPrice form-control"
                                   placeholder="Giá sản phẩm" style="display: block"/>
                        </div>
                        <div class="form-group m-r-10">
                            <button class="btn  btn-primary" id="add-row">Thêm</button>
                        </div>
                    </div>
                    <div class="col-md-12 form-order">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                <input type="checkbox" value="1" name="orderType" class="orderType form-control" id="orderType" />
                                Đơn hàng online</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" value="" name="shippingFee" class="shippingFee form-control" placeholder="phí vận chuyển"/>
                        </div>
                        <div class="form-group payMethod" style="display: none;">
                            <select name="paymentMethod" class="form-control paymentMethod">
                                <option value=""> Chọn hình thức chuyển tiền </option>
                                <option value="1"> Trả trực tiếp </option>
                                <option value="2"> Chuyển khoản </option>
                                <option value="3"> COD </option>
                            </select>
                            <input type="text" value="" name="orderNote" class="orderNote form-control" placeholder="Ghi chú"/>
                        </div>
                        <input type="hidden" name="hiddenship" value="0" class="hiddenShip" />
                    </div>
                </form>

                <table class="table table-bordered form-order" id="table-order">
                    <thead>
                    <tr>
                        <th>Mã sách</th>
                        <th>Tên sách</th>
                        <th>Số lượng</th>
                        <th>Đơn giá</th>
                        <th>Thành tiền</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

                <form class="form-inline">
                    <div class="form-group">
                        <label for="totalPrice">Tổng tiền: </label>
                        <input type="text" class="form-control totalPrice" name="totalPrice" value="0"/>
                    </div>
                    <div class="form-group">
                        <label for="acceptPrice">Nhận từ khách: </label>
                        <input type="text" class="form-control acceptPrice" name="acceptPrice" value="0"/>
                    </div>
                    <div class="form-group">
                        <label for="reAmount">Trả lại: </label>
                        <input type="text" class="form-control reAmount" name="reAmount" value="0"/>
                    </div>
                    <div class="form-group m-r-10">
                        <button class="apply-order btn btn-success">Xác nhận</button>
                    </div>
                    <input type="hidden" name="arrBookCode" class="arrBookCode" value=""/>
                </form>

            </div>
        </div>
    </div>
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default"
                       href="javascript:;"><i class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger"
                       href="javascript:;"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $this->pageTitle; ?></h4>
            </div>
            <div class="panel-body" style="display: block;">
                <?php $this->renderPartial('_search', array(
                    'model' => $model,
                )); ?>
                <div class="row" style="margin: 10px 0 0 0;">
                    <?php if(!empty($model->orderPhone) || !empty($model->ordername) || !empty($model->orderEmail)){
                            $data = $model->search()->getData();
                            
                            echo CHtml::button('Thêm đơn hàng cho khách hàng', array('class' => 'btn btn-success btn-add-customerorder', 'data' => $data[0]->customerCode));
                    }?>
                </div>
                <?php
                
                $form = $this->beginWidget('CActiveForm', array(
                    'enableAjaxValidation' => true,
                ));
            
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'purchasebooks-grid',
                    'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                    'summaryText' => 'Hiển thị danh sách từ {start}-{end} trong tổng số {count} kết quả',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'columns' => array(
                        array(
                            'id' => 'autoId',
                            'class' => 'CCheckBoxColumn',
                            'selectableRows' => '50',
                        ),
                        array(
                            'name' => 'orderCode',
                            'type' => 'raw',
                            'value' => 'CHtml::link("$data->orderCode", array("purchasebooks/view","id" => $data->id))'
                        ),
                        'orderAt',
                        'customerCode',
                        array(
                            'name' => 'totalAmount',
                            'type' => 'raw',
                            'value' => 'number_format($data->totalAmount)'
                        ),
                        array(
                            'name' => 'orderType',
                            'type' => 'raw',
                            'value' => '($data->orderType == 1) ? "online" : "offline"'
                        ),
                        /*
                        'transportMethod',
                        'orderStatus',
                        'shippingAddess',
                        'note',
                        */
                       /* array(
                            'class' => 'CButtonColumn',
                            'template' => '{update}'
                        ), */
                    ),
                ));?>
                <script>
                    function reloadGrid(data) {
                        $.fn.yiiGridView.update('purchasebooks-grid');
                    }
                </script>
                <?php
                echo CHtml::ajaxSubmitButton('Xoá đơn hàng',
                    array('purchasebooks/ajaxupdate', 'act' => 'doDelete'),
                    array('success' => 'function(data){
                                obj = JSON.parse(data);
                                    alert(obj.msg);
                                    location.reload();
                                    //reloadGrid
                            }'),
                    array('class' => 'btn-ajax btn btn-warning', "onclick" => "confirm('Bạn muốn xoá các đơn hàng được chọn?')"));

                $this->endWidget();
                ?>
            </div>
        </div>
    </div>
    </div>
<?php
$function = new Functions;
Yii::app()->clientScript->registerScript('purchasebooks', "
    var _customerCode = '" . $function->generateTableCode("customer") . "';
    $('.customerCode').val(_customerCode);

    $('#add-row').click(function(e){
            e.preventDefault();
             var _hasShipFee = $('.hiddenShip').val();
             var _cusCode = $('.customerCode').val();
             var _cusName = $('.customerName').val();
             var _bookCode = $('.bookCode').val();
             var _bookName = $('.search-bookname').val();
             var _bookQuantity = $('.bookQuantity').val();
             var _arrBookCode = $('.arrBookCode').val();

             var _addPrice = $('.bookPrice').val();
             var _shippingFee = $('.shippingFee').val();
             var _orderType = 0;
            //$('.totalPrice').val('');
            //console.log('totalAmount: ' + _finalPrice);

        if(_bookName != '' && _bookCode != '' && _bookQuantity != ''){

            if(_addPrice != 0){
                var findme = '.';

                if ( check(_addPrice) == false) {
                    var _strMsgPrice = '<strong>Cảnh báo! </strong> Giá sản phầm không hợp lệ. Giá có chứa ký tự đặc biệt. Vui lòng kiểm tra.';
                    $('#add-purchase-alert').html(_strMsgPrice);
                    $('#add-purchase-alert').removeAttr('style');
                    setTimeout(function(){ $('#add-purchase-alert').fadeOut() }, 5000);

                } else {
                    var _bookPrice = parseInt($('.bookPrice').val()) * _bookQuantity;
                    console.log('bookprice with quantity: ' + _bookPrice);
                    console.log('total Price: ' + $('.totalPrice').val());
                    var _finalPrice = parseInt($('.totalPrice').val()) + parseInt(_bookPrice);

                    var _finalBookArr = _arrBookCode + _bookCode + ':' + _bookQuantity +',';

                    if($('#orderType').is(':checked') && _hasShipFee == 0 && _shippingFee > 0){
                        //console.log('orderType is checked');

                        _orderType = $('#orderType').val();

                       // console.log('orderType: ' + _shippingFee + ' -- ' + _orderType);

                        var _withShipFee = _finalPrice + parseInt(_shippingFee);
                        //console.log('totalAmount: ' + _finalPrice + ' + ShipFee: ' + _shippingFee + ' = ' + _withShipFee);
                        $('.totalPrice').val(_withShipFee);

                         $('.hiddenShip').val('1');
                    }else{
                         $('.totalPrice').val(_finalPrice);
                    }

                console.log('Final Price: ' + _finalPrice);

                $('.arrBookCode').val(_finalBookArr);
                console.log(_finalBookArr);

                $.post('" . Yii::app()->createUrl('purchasebooks/updatebookprice') . "', {bookprice: _addPrice, bookcode: _bookCode},function(data){
                    console.log(data);
                });

                var _addtr = '<tr>'
                              +'<td>' + _bookCode + '</td>'
                              +'<td>' + _bookName + '</td>'
                              +'<td>' + _bookQuantity + '</td>'
                              +'<td>' + _addPrice + '</td>'
                              +'<td>' + _bookPrice + '</td>'
                              +'<td> <a href=\"\" class=\"delete-order\">Xoá</a></td>'
                              +'</tr>';
                $('#table-order > tbody:last-child').append(_addtr);


                // Reset form.
                $('.bookCode').val('');
                $('.search-bookname').val('');
                $('.bookQuantity').val('');

                $('.bookPrice').val('');
                //$('.bookPrice').hide();
                }

            }else{
                var _strMsgPrice = '<strong>Cảnh báo! </strong> Giá sản phầm không hợp lệ. Hiện tại đang bằng 0. Vui lòng kiểm tra.';
                $('#add-purchase-alert').html(_strMsgPrice);
                $('#add-purchase-alert').removeAttr('style');
                setTimeout(function(){ $('#add-purchase-alert').fadeOut() }, 5000);
            }
        }else{
            var _strMsg = '<strong>Cảnh báo! </strong> Tất cả các trường <strong>Mã sách</strong>, <strong>Tên sách</strong>, <strong>Số lượng</strong>, <strong>Giá sách</strong> không được để trống. Nếu bạn thấy cảnh báo này hãy kiểm tra lại.';
            $('#add-purchase-alert').html(_strMsg);
            $('#add-purchase-alert').removeAttr('style');
            setTimeout(function(){ $('#add-purchase-alert').fadeOut() }, 5000);
        }
        });

    $('.apply-order').click(function(e){
        e.preventDefault();
        var _arrBookCode = $('.arrBookCode').val();
        var _cusCode = $('.customerCode').val();
        var _cusName = $('.customerName').val();
        var _cusPhone = $('.customePhone').val();
        var _cusEmail = $('.customerEmail').val();
        var _cusAddress = $('.customerAddress').val();

        var _totalAmount = $('.totalPrice').val();

        var _shippingFee = 0;
        var _orderType = 0;
        var _orderStatus = 1;
        var _paymentMethod = 1;
        var _orderNote = '';


        if($('#orderType').is(':checked')){
            _shippingFee = $('.shippingFee').val();
            _orderType = $('#orderType').val();
            _orderStatus = 0;
            _paymentMethod = $('.paymentMethod option:selected').val();
            _orderNote = $('.orderNote').val();
        }

        var _data = {
            'cuscode' : _cusCode,
            'cusname' : _cusName,
            'cusphone' : _cusPhone,
            'cusemail' : _cusEmail,
            'cusaddress' : _cusAddress,
            'totalAmount' : _totalAmount,
            'arrBooks' : _arrBookCode,
            'orderType' : _orderType,
            'shippingFee': _shippingFee,
            'orderStatus' : _orderStatus,
            'paymentMethod' : _paymentMethod,
            'orderNote': _orderNote
        };

        $.post('" . Yii::app()->createUrl('purchasebooks/applyorder') . "', {data: _data}, function(data){
            var objreturn = jQuery.parseJSON( data );
            if(objreturn.status){
                location.reload();
            }
        });

    });

    $('.search-bookname').change(function(){
        var _bookNameVal = $(this).val();
        $.post('" . $this->createUrl("purchasebooks/ajaxgetbookcode") . "',{bookname: _bookNameVal}, function(returncode){
            //console.log(returncode);
            var obj = jQuery.parseJSON( returncode );
            if(!obj.existbook){
                $('.bookCode').val(obj.bookcode);
                $('.bookPrice').val(obj.bookprice);
                $('.bookPrice').show();
                $('.bookQuantity').val('1');
            }
        });
    });

    $('.acceptPrice').change(function(){
        var _acceptPrice = parseInt($(this).val());
        var _totalPrice = parseInt($('.totalPrice').val());

        if(_acceptPrice >=  _totalPrice){
            var _reAmount = _acceptPrice - _totalPrice;

            $('.reAmount').val('-'+_reAmount);
        }
    });
    $('.delete-order').live('click', function(event) {
        event.preventDefault();
        var _row = $(this).closest('tr');    // Find the row
        var _rowTotalAmount = _row.find('td:eq(4)').text();

        var _rowBookCode =  _row.find('td:eq(0)').text();
        var _rowBookQuantity = _row.find('td:eq(2)').text();

        var _arrBookRemove = _rowBookCode +':'+ _rowBookQuantity + ',';

        var _totalAmount = $('.totalPrice').val();
        var _finalBookArr = $('.arrBookCode').val();

        var _rmBookArr = _finalBookArr.replace(_arrBookRemove, '');

        $('.arrBookCode').val(_rmBookArr);
        _totalAmount = _totalAmount - parseInt(_rowTotalAmount);
        $('.totalPrice').val(_totalAmount);

       $(this).parent().parent().remove();
    });

    var specialChars = '<>@!#$%^&*()_+[]{}?:;|\',./~`-='
    var check = function(string){
        for(i = 0; i < specialChars.length;i++){
            if(string.indexOf(specialChars[i]) > -1){
                return false;
        }
     }
        return true;
    }

    $('#orderType').change(function() {
        if($(this).is(':checked')) {
            $('.payMethod').removeAttr('style');
            $('.shippingFee').val('0');
        }else{
            var _shipFee = parseInt($('.shippingFee').val());
            var _totalPrice = parseInt($('.totalPrice').val());

            if(_totalPrice > 0 && _totalPrice >= _shipFee){
                $('.totalPrice').val(_totalPrice - _shipFee);
            }

            $('.payMethod').css('display','none');
            $('.shippingFee').val('');
            $('.hiddenShip').val('0');

        }
    });

    $('.btn-add-customerorder').click(function(e){
        e.preventDefault();
        var _dataCode = $(this).attr('data');
        
        $.post('".Yii::app()->createUrl("purchasebooks/addtoorder")."', {submit: true, data: _dataCode}, function(custdata){
            var _cusObj = jQuery.parseJSON(custdata);
           
            if(_cusObj.exist){
                $('.customerCode').val(_cusObj.custcode);
                $('.customerName').val(_cusObj.custname);
                $('.customePhone').val(_cusObj.custphone);
                $('.customerEmail').val(_cusObj.custemail);
                $('.customerAddress').val(_cusObj.custaddress);
            }
            $('.btn-scroll-to-top').trigger('click');
        });
        
    });
");
?>