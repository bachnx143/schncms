<div class="row">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array(
        'class' => 'form-inline form-bordered',
    )
)); ?>
	<div class="form-group">
		<?php echo $form->textField($model,'orderCode',array('class' => 'form-control', 'placeholder' => 'Mã đơn hàng')); ?>
	</div>
    
    <div class="form-group">
		<?php echo $form->textField($model,'ordername',array('class' => 'form-control', 'placeholder' => 'Tên khách hàng')); ?>
	</div>
    
    <div class="form-group">
		<?php echo $form->textField($model,'orderPhone',array('class' => 'form-control', 'placeholder' => 'Số điện thoại khách hàng')); ?>
	</div>
    
    <div class="form-group">
		<?php echo $form->textField($model,'orderEmail',array('class' => 'form-control', 'placeholder' => 'E-mail khách hàng')); ?>
	</div>
    
	<!--<div class="form-group">
		<?php //echo $form->textField($model,'customerCode',array('class' => 'form-control', 'placeholder' => 'Mã khách hàng')); ?>
	</div> -->

	<div class="form-group">
        <?php $listOrderStatus = array(0=>"Chưa thanh toán", 1=>"Đã thanh toán"); ?>
		<?php echo $form->dropDownList($model,'orderStatus', $listOrderStatus, array('class' => 'form-control' , 'empty' => 'Trạng thái đơn hàng')); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Tìm kiếm', array('class' => 'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>