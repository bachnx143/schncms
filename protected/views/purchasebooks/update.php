<?php
/* @var $this PurchasebooksController */
/* @var $model Purchasebooks */

$this->breadcrumbs=array(
	'Purchasebooks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Purchasebooks', 'url'=>array('index')),
	array('label'=>'Create Purchasebooks', 'url'=>array('create')),
	array('label'=>'View Purchasebooks', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Purchasebooks', 'url'=>array('admin')),
);
?>

<h1>Update Purchasebooks <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>