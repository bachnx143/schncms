<?php
/* @var $this PurchasebooksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Purchasebooks',
);

$this->menu=array(
	array('label'=>'Create Purchasebooks', 'url'=>array('create')),
	array('label'=>'Manage Purchasebooks', 'url'=>array('admin')),
);
?>

<h1>Purchasebooks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
