<?php
/* @var $this PurchasebooksController */
/* @var $model Purchasebooks */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchasebooks-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'orderCode'); ?>
		<?php echo $form->textField($model,'orderCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'orderCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderAt'); ?>
		<?php echo $form->textField($model,'orderAt'); ?>
		<?php echo $form->error($model,'orderAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customerId'); ?>
		<?php echo $form->textField($model,'customerId'); ?>
		<?php echo $form->error($model,'customerId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customerCode'); ?>
		<?php echo $form->textField($model,'customerCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'customerCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paymentMethod'); ?>
		<?php echo $form->textField($model,'paymentMethod'); ?>
		<?php echo $form->error($model,'paymentMethod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'transportMethod'); ?>
		<?php echo $form->textField($model,'transportMethod'); ?>
		<?php echo $form->error($model,'transportMethod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderStatus'); ?>
		<?php echo $form->textField($model,'orderStatus'); ?>
		<?php echo $form->error($model,'orderStatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shippingAddess'); ?>
		<?php echo $form->textField($model,'shippingAddess',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'shippingAddess'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->