<?php
/* @var $this PurchasebooksController */
/* @var $model Purchasebooks */

$this->breadcrumbs=array(
	'Purchasebooks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Purchasebooks', 'url'=>array('index')),
	array('label'=>'Manage Purchasebooks', 'url'=>array('admin')),
);
?>

<h1>Create Purchasebooks</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>