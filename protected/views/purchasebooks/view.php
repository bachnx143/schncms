<?php
/* @var $this PurchasebooksController */
/* @var $model Purchasebooks */

$this->breadcrumbs=array(
	'Purchasebooks'=>array('index'),
	$model->id,
);
?>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">

            </div>
            <div class="panel-body" style="display: block">
                <div class="col-md-6">
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('orderCode'); ?> : <?php echo $model->orderCode; ?></p>
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('customerCode'); ?> : <?php echo CHtml::link($model->customerCode, array('customers/viewcustomer', 'customercode' => $model->customerCode)); ?></p>
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('orderAt'); ?> : <?php echo date("d-m-Y H:i:s", strtotime($model->orderAt)); ?></p>
                </div>
                <div class="col-md-6">
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('orderType'); ?> : <?php echo ($model->orderType == 1) ? "Online" : "Offline"; ?></p>
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('orderStatus'); ?> : <?php echo ($model->orderStatus == 1) ? "Đã thanh toán" : "Chưa thanh toán"; ?></p>
                    <p><?php echo  Purchasebooks::model()->getAttributeLabel('shippingFee'); ?> : <?php echo number_format($model->shippingFee); ?></p>
                </div>
                <p>Sản phẩm</p>
                <?php $findAllBook = Orderlist::model()->findAll("purchaseCode = :purchasecode", array(":purchasecode" => $model->orderCode));?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Mã sách</th>
                        <th>Tên sách</th>
                        <th>Số lượng</th>
                        <th>Đơn giá</th>
                        <th>Thành tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $total = 0;
                        $i = 0;
                    foreach($findAllBook as $itemBook): ?>
                       <?php if($i % 2 == 0): ?>
                            <tr class="info">
                                <td><?php echo $itemBook->bookCode; ?></td>
                                <td><?php echo Functions::getBookInfo($itemBook->bookCode, "bookName"); ?></td>
                                <td><?php echo $quanity = $itemBook->quantity; ?></td>
                                <td><?php echo $price = Functions::getBookInfo($itemBook->bookCode, "exportPrice"); ?></td>
                                <td><?php
                                    $amount = ($price * $quanity);
                                    $total = $total + $amount;
                                    echo number_format($amount);
                                    ?></td>
                            </tr>
                       <?php else: ?>
                            <tr class="warning">
                                <td><?php echo $itemBook->bookCode; ?></td>
                                <td><?php echo Functions::getBookInfo($itemBook->bookCode, "bookName"); ?></td>
                                <td><?php echo $quanity = $itemBook->quantity; ?></td>
                                <td><?php echo $price = Functions::getBookInfo($itemBook->bookCode, "exportPrice"); ?></td>
                                <td><?php
                                    $amount = ($price * $quanity);
                                    $total = $total + $amount ;
                                    echo number_format($amount);
                                    ?></td>
                            </tr>
                            <?php endif;?>
                    <?php $i++; endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4">Tổng tiền </td>
                        <td><?php echo number_format($total); ?></td>
                    </tr>
                    <?php if($model->orderType == 1): ?>
                        <tr>
                            <td colspan="4">Tổng tiền (Đã bao gồm phí vận chuyển) </td>
                            <td><?php echo number_format($total + $model->shippingFee); ?></td>
                        </tr>
                    <?php endif; ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

