<?php
/* @var $this PurchasebooksController */
/* @var $data Purchasebooks */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderCode')); ?>:</b>
	<?php echo CHtml::encode($data->orderCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderAt')); ?>:</b>
	<?php echo CHtml::encode($data->orderAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customerId')); ?>:</b>
	<?php echo CHtml::encode($data->customerId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customerCode')); ?>:</b>
	<?php echo CHtml::encode($data->customerCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentMethod')); ?>:</b>
	<?php echo CHtml::encode($data->paymentMethod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transportMethod')); ?>:</b>
	<?php echo CHtml::encode($data->transportMethod); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('orderStatus')); ?>:</b>
	<?php echo CHtml::encode($data->orderStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shippingAddess')); ?>:</b>
	<?php echo CHtml::encode($data->shippingAddess); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	*/ ?>

</div>