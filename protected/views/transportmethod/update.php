<?php
/* @var $this TransportmethodController */
/* @var $model Transportmethod */

$this->breadcrumbs=array(
	'Transportmethods'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Transportmethod', 'url'=>array('index')),
	array('label'=>'Create Transportmethod', 'url'=>array('create')),
	array('label'=>'View Transportmethod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Transportmethod', 'url'=>array('admin')),
);
?>

<h1>Update Transportmethod <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>