<?php
/* @var $this TransportmethodController */
/* @var $data Transportmethod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transMethodCode')); ?>:</b>
	<?php echo CHtml::encode($data->transMethodCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transMethodName')); ?>:</b>
	<?php echo CHtml::encode($data->transMethodName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transMethodFee')); ?>:</b>
	<?php echo CHtml::encode($data->transMethodFee); ?>
	<br />


</div>