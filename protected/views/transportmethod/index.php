<?php
/* @var $this TransportmethodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Transportmethods',
);

$this->menu=array(
	array('label'=>'Create Transportmethod', 'url'=>array('create')),
	array('label'=>'Manage Transportmethod', 'url'=>array('admin')),
);
?>

<h1>Transportmethods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
