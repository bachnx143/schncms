<?php
/* @var $this TransportmethodController */
/* @var $model Transportmethod */

$this->breadcrumbs=array(
	'Transportmethods'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Transportmethod', 'url'=>array('index')),
	array('label'=>'Create Transportmethod', 'url'=>array('create')),
	array('label'=>'Update Transportmethod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Transportmethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Transportmethod', 'url'=>array('admin')),
);
?>

<h1>View Transportmethod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'transMethodCode',
		'transMethodName',
		'description',
		'transMethodFee',
	),
)); ?>
