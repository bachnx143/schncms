<?php
/* @var $this TransportmethodController */
/* @var $model Transportmethod */

$this->breadcrumbs=array(
	'Transportmethods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Transportmethod', 'url'=>array('index')),
	array('label'=>'Manage Transportmethod', 'url'=>array('admin')),
);
?>

<h1>Create Transportmethod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>