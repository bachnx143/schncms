<?php
/* @var $this RolesController */
/* @var $data Roles */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roleCode')); ?>:</b>
	<?php echo CHtml::encode($data->roleCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roleName')); ?>:</b>
	<?php echo CHtml::encode($data->roleName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descriptions')); ?>:</b>
	<?php echo CHtml::encode($data->descriptions); ?>
	<br />


</div>