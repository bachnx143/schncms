<?php
/* @var $this RolesController */
/* @var $model Roles */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'roles-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'roleCode'); ?>
		<?php echo $form->textField($model,'roleCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'roleCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'roleName'); ?>
		<?php echo $form->textField($model,'roleName',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'roleName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descriptions'); ?>
		<?php echo $form->textField($model,'descriptions',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'descriptions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->