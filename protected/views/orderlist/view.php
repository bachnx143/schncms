<?php
/* @var $this OrderlistController */
/* @var $model Orderlist */

$this->breadcrumbs=array(
	'Orderlists'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Orderlist', 'url'=>array('index')),
	array('label'=>'Create Orderlist', 'url'=>array('create')),
	array('label'=>'Update Orderlist', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Orderlist', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Orderlist', 'url'=>array('admin')),
);
?>

<h1>View Orderlist #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customerCode',
		'bookCode',
		'createdAt',
		'note',
	),
)); ?>
