<?php
/* @var $this OrderlistController */
/* @var $model Orderlist */

$this->breadcrumbs=array(
	'Orderlists'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Orderlist', 'url'=>array('index')),
	array('label'=>'Create Orderlist', 'url'=>array('create')),
	array('label'=>'View Orderlist', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Orderlist', 'url'=>array('admin')),
);
?>

<h1>Update Orderlist <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>