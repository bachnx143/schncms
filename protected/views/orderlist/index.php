<?php
/* @var $this OrderlistController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orderlists',
);

$this->menu=array(
	array('label'=>'Create Orderlist', 'url'=>array('create')),
	array('label'=>'Manage Orderlist', 'url'=>array('admin')),
);
?>

<h1>Orderlists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
