<?php
/* @var $this OrderlistController */
/* @var $model Orderlist */

$this->breadcrumbs=array(
	'Orderlists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Orderlist', 'url'=>array('index')),
	array('label'=>'Manage Orderlist', 'url'=>array('admin')),
);
?>

<h1>Create Orderlist</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>