<?php
/* @var $this PaymentmethodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Paymentmethods',
);

$this->menu=array(
	array('label'=>'Create Paymentmethod', 'url'=>array('create')),
	array('label'=>'Manage Paymentmethod', 'url'=>array('admin')),
);
?>

<h1>Paymentmethods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
