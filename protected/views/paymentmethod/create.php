<?php
/* @var $this PaymentmethodController */
/* @var $model Paymentmethod */

$this->breadcrumbs=array(
	'Paymentmethods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Paymentmethod', 'url'=>array('index')),
	array('label'=>'Manage Paymentmethod', 'url'=>array('admin')),
);
?>

<h1>Create Paymentmethod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>