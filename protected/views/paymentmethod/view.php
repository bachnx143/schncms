<?php
/* @var $this PaymentmethodController */
/* @var $model Paymentmethod */

$this->breadcrumbs=array(
	'Paymentmethods'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Paymentmethod', 'url'=>array('index')),
	array('label'=>'Create Paymentmethod', 'url'=>array('create')),
	array('label'=>'Update Paymentmethod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Paymentmethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Paymentmethod', 'url'=>array('admin')),
);
?>

<h1>View Paymentmethod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'methodCode',
		'methodName',
		'description',
	),
)); ?>
