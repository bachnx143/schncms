<?php
/* @var $this PaymentmethodController */
/* @var $model Paymentmethod */

$this->breadcrumbs=array(
	'Paymentmethods'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Paymentmethod', 'url'=>array('index')),
	array('label'=>'Create Paymentmethod', 'url'=>array('create')),
	array('label'=>'View Paymentmethod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Paymentmethod', 'url'=>array('admin')),
);
?>

<h1>Update Paymentmethod <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>