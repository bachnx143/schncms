<?php
/* @var $this PaymentmethodController */
/* @var $data Paymentmethod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('methodCode')); ?>:</b>
	<?php echo CHtml::encode($data->methodCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('methodName')); ?>:</b>
	<?php echo CHtml::encode($data->methodName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>