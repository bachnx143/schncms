<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manageaccounts-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'class' => 'form-horizontal'
    )
)); ?>
    <div id="add-accounts-alert" class="alert alert-warning fade in m-b-15" style="display: none;">
        <span class="alt-content">
            <strong>Warning!</strong>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </span>
        <span data-dismiss="alert" class="close">×</span>
    </div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'accountCode', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->textField($model,'accountCode',array('class' => 'form-control accountCode', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model,'accountCode'); ?>
        </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'accountLogin', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model,'accountLogin',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'accountLogin'); ?>
        </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'accountSecret', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->passwordField($model,'accountSecret',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'accountSecret'); ?>
        </div>
	</div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'accountReSecret', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->passwordField($model,'accountReSecret',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'accountReSecret'); ?>
        </div>
    </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'accountName', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model,'accountName',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'accountName'); ?>
        </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'roleCode', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php //if(Yii::app()->user->roles == "administrator"): ?>
                <?php $listroles = CHtml::listData(Roles::model()->findAll(), 'roleCode','roleName');?>
            <?php //else: ?>
                <?php //$listroles = CHtml::listData(Roles::model()->findAll("roleName != :name", array(":name" => 'administrator')), 'roleCode','roleName');?>
            <?php //endif; ?>
            <?php echo $form->dropDownList($model,'roleCode', $listroles, array('class' => 'form-control', 'empty' => 'Chọn quyền truy cập')); ?>
            <?php echo $form->error($model,'roleCode'); ?>
        </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'accountEmail', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model,'accountEmail',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'accountEmail'); ?>
        </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'phoneNumber', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model,'phoneNumber',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'phoneNumber'); ?>
        </div>
	</div>

	<div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-9">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo tài khoản' : 'Cập nhật tài khoản', array('class' => 'btn btn-success')); ?>
        </div>
	</div>

<?php $this->endWidget(); ?>