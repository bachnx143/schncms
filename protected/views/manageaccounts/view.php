<?php
/* @var $this ManageaccountsController */
/* @var $model Manageaccounts */

$this->breadcrumbs=array(
	'Manageaccounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Manageaccounts', 'url'=>array('index')),
	array('label'=>'Create Manageaccounts', 'url'=>array('create')),
	array('label'=>'Update Manageaccounts', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Manageaccounts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manageaccounts', 'url'=>array('admin')),
);
?>

<h1>View Manageaccounts #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'accountCode',
		'accountLogin',
		'accountSecret',
		'accountName',
		'createdAt',
		'roleCode',
		'accountEmail',
		'phoneNumber',
		'activeStatus',
		'accountSecretKey',
		'lastLoginAt',
		'lastLoginIn',
		'onLogin',
	),
)); ?>
