<?php
/* @var $this ManageaccountsController */
/* @var $model Manageaccounts */

$this->breadcrumbs=array(
	'Manageaccounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Manageaccounts', 'url'=>array('index')),
	array('label'=>'Create Manageaccounts', 'url'=>array('create')),
	array('label'=>'View Manageaccounts', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Manageaccounts', 'url'=>array('admin')),
);
?>

<h1>Update Manageaccounts <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>