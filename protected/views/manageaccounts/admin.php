<?php
/* @var $this ManageaccountsController */
/* @var $model Manageaccounts */

$this->breadcrumbs=array(
	'Manageaccounts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Manageaccounts', 'url'=>array('index')),
	array('label'=>'Create Manageaccounts', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manageaccounts-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $this->pageTitle; ?></h4>
            </div>
            <div class="panel-body" style="display: block;">
                <?php echo CHtml::link('Thêm tài khoản', array('manageaccounts/create'), array('class' => 'btn btn-primary btn-addcate')); ?>
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'manageaccounts-grid',
                    'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                    'summaryText' => 'Hiển thị danh sách từ {start}-{end} trong tổng số {count} kết quả',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'columns'=>array(
                        array(
                            'id' => 'autoId',
                            'class' => 'CCheckBoxColumn',
                            'selectableRows' => '50',
                        ),
                        'accountCode',
                        'accountLogin',
                        'accountName',
                        'createdAt',
                        array(
                            'name' => 'roleCode',
                            //'type' => 'raw',
                            'value' => 'Manageaccounts::model()->getAccountInfo($data->accountLogin, true, "roleName")' //'$data->roles->roleName'
                        ),
                        'lastLoginAt',
                        'lastLoginIn',
                        /*
                        'accountEmail',
                        'phoneNumber',
                        'activeStatus',
                        'accountSecretKey',
                        'lastLoginIn',
                        'onLogin',
                        */
                        array(
                            'class'=>'CButtonColumn',
                            'template' => '{update}'
                        ),
                    ),
                )); ?>
            </div>
        </div>
    </div>
</div>

