<?php
/* @var $this ManageaccountsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manageaccounts',
);

$this->menu=array(
	array('label'=>'Create Manageaccounts', 'url'=>array('create')),
	array('label'=>'Manage Manageaccounts', 'url'=>array('admin')),
);
?>

<h1>Manageaccounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
