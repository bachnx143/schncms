<?php
/* @var $this ManageaccountsController */
/* @var $data Manageaccounts */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountCode')); ?>:</b>
	<?php echo CHtml::encode($data->accountCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountLogin')); ?>:</b>
	<?php echo CHtml::encode($data->accountLogin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountSecret')); ?>:</b>
	<?php echo CHtml::encode($data->accountSecret); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountName')); ?>:</b>
	<?php echo CHtml::encode($data->accountName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdAt')); ?>:</b>
	<?php echo CHtml::encode($data->createdAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roleCode')); ?>:</b>
	<?php echo CHtml::encode($data->roleCode); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('accountEmail')); ?>:</b>
	<?php echo CHtml::encode($data->accountEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phoneNumber')); ?>:</b>
	<?php echo CHtml::encode($data->phoneNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activeStatus')); ?>:</b>
	<?php echo CHtml::encode($data->activeStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountSecretKey')); ?>:</b>
	<?php echo CHtml::encode($data->accountSecretKey); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastLoginAt')); ?>:</b>
	<?php echo CHtml::encode($data->lastLoginAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastLoginIn')); ?>:</b>
	<?php echo CHtml::encode($data->lastLoginIn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('onLogin')); ?>:</b>
	<?php echo CHtml::encode($data->onLogin); ?>
	<br />

	*/ ?>

</div>