<?php
/* @var $this ManageaccountsController */
/* @var $model Manageaccounts */

$this->breadcrumbs=array(
	'Manageaccounts'=>array('index'),
	'Create',
);
?>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">

            </div>
            <div class="panel-body" style="display: block">
                <?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
    </div>
</div>
<?php
$function = new Functions;
$accCode = $function->generateTableCode("account");

Yii::app()->clientScript->registerScript('createAccounts', "
	var accCodetime = '".$accCode."';

	$('.accountCode').val(accCodetime);

    $('.books-quantity').change(function(){
        var _bookImportprice = $('.book-import-price').val();
        var _bookQuantity = $(this).val();

        var _totalAmount = _bookImportprice * _bookQuantity;
        console.log(_bookImportprice * _bookQuantity);

        $('.book-total-amount').val(_totalAmount);
    });
	/**  Validate books form. */
	$('#manageaccounts-form').validate({
                onfocusout: true,
                onkeyup: true,
                onclick: true,
                focusInvalid: true,
                debug:true,
                rules:{
                    'Manageaccounts[accountLogin]': {required: true, remote:{url: '".Yii::app()->createUrl('manageaccounts/checkaccount')."', type: 'post'}},
                    'Manageaccounts[accountSecret]': {required: true, minlength: 6},
                    'Manageaccounts[accountReSecret]': {equalTo: '#Manageaccounts_accountSecret'},
                    'Manageaccounts[roleCode]': 'required'
                },
                messages:{
                    'Manageaccounts[accountLogin]': {required: 'Vui lòng nhập tên tài khoản đầy đủ.', remote: 'Tài khoản đã tồn tại. Vui lòng chọn tài khoản khác'},
                    'Manageaccounts[accountSecret]': {required: 'Vui lòng nhập mật khẩu. ', minlength: 'Mật khẩu tối thiểu chứa 6 ký tự'},
                    'Manageaccounts[accountReSecret]': {equalTo: 'Mật khẩu nhập lại không khớp. Vui lòng kiểm tra lại.'},
                    'Manageaccounts[roleCode]': 'Chưa chọn quyền truy cập. Vui lòng chọn.'
                },
                submitHandler: function(){
                    var formData = new FormData($('#manageaccounts-form')[0]);
                    var action = $('#manageaccounts-form').attr('action');

                       $.ajax({
                            url: action,
                            type: 'POST',
                            data: formData,
                            async: false,
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                if(obj.status){
                                    $('.alt-content').html(obj.msg);
                                    $('#add-accounts-alert').removeAttr('style');
                                    $('#add-accounts-alert').removeClass('alert-warning');
                                    $('#add-accounts-alert').addClass('alert-success');
                                    $('#manageaccounts-form')[0].reset();

                                    setTimeout(function(){ $('#add-accounts-alert').fadeOut() }, 5000);

                                    $.get('".Yii::app()->createUrl('manageaccounts/getacccode')."', {}, function(returncode){
                                        $('#Manageaccounts_accountCode').val(returncode);
                                    });

                                }else{
                                    $('.alt-content').html(obj.msg);
                                    $('#add-accounts-alert').removeClass('alert-success');
                                    $('#add-accounts-alert').addClass('alert-warning');
                                    $('#add-accounts-alert').removeAttr('style');
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                }
            });
");
?>