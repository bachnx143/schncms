<?php
/* @var $this ManageaccountsController */
/* @var $model Manageaccounts */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountCode'); ?>
		<?php echo $form->textField($model,'accountCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountLogin'); ?>
		<?php echo $form->textField($model,'accountLogin',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountSecret'); ?>
		<?php echo $form->textField($model,'accountSecret',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountName'); ?>
		<?php echo $form->textField($model,'accountName',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdAt'); ?>
		<?php echo $form->textField($model,'createdAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'roleCode'); ?>
		<?php echo $form->textField($model,'roleCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountEmail'); ?>
		<?php echo $form->textField($model,'accountEmail',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phoneNumber'); ?>
		<?php echo $form->textField($model,'phoneNumber',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activeStatus'); ?>
		<?php echo $form->textField($model,'activeStatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountSecretKey'); ?>
		<?php echo $form->textField($model,'accountSecretKey',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastLoginAt'); ?>
		<?php echo $form->textField($model,'lastLoginAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastLoginIn'); ?>
		<?php echo $form->textField($model,'lastLoginIn',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'onLogin'); ?>
		<?php echo $form->textField($model,'onLogin'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->