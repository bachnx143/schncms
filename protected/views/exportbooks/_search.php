<?php
/* @var $this ExportbooksController */
/* @var $model Exportbooks */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bookCode'); ?>
		<?php echo $form->textField($model,'bookCode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exportedAt'); ?>
		<?php echo $form->textField($model,'exportedAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exportReason'); ?>
		<?php echo $form->textField($model,'exportReason',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exportedBy'); ?>
		<?php echo $form->textField($model,'exportedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exportedByUser'); ?>
		<?php echo $form->textField($model,'exportedByUser',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'totalAmount'); ?>
		<?php echo $form->textField($model,'totalAmount'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->