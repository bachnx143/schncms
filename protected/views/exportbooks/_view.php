<?php
/* @var $this ExportbooksController */
/* @var $data Exportbooks */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookCode')); ?>:</b>
	<?php echo CHtml::encode($data->bookCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exportedAt')); ?>:</b>
	<?php echo CHtml::encode($data->exportedAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exportReason')); ?>:</b>
	<?php echo CHtml::encode($data->exportReason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exportedBy')); ?>:</b>
	<?php echo CHtml::encode($data->exportedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exportedByUser')); ?>:</b>
	<?php echo CHtml::encode($data->exportedByUser); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalAmount')); ?>:</b>
	<?php echo CHtml::encode($data->totalAmount); ?>
	<br />

	*/ ?>

</div>