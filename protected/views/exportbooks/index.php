<?php
/* @var $this ExportbooksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Exportbooks',
);

$this->menu=array(
	array('label'=>'Create Exportbooks', 'url'=>array('create')),
	array('label'=>'Manage Exportbooks', 'url'=>array('admin')),
);
?>

<h1>Exportbooks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
