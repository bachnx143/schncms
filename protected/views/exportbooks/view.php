<?php
/* @var $this ExportbooksController */
/* @var $model Exportbooks */

$this->breadcrumbs=array(
	'Exportbooks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Exportbooks', 'url'=>array('index')),
	array('label'=>'Create Exportbooks', 'url'=>array('create')),
	array('label'=>'Update Exportbooks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Exportbooks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Exportbooks', 'url'=>array('admin')),
);
?>

<h1>View Exportbooks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'bookCode',
		'quantity',
		'exportedAt',
		'exportReason',
		'exportedBy',
		'exportedByUser',
		'totalAmount',
	),
)); ?>
