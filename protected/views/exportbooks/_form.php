<?php
/* @var $this ExportbooksController */
/* @var $model Exportbooks */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exportbooks-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bookCode'); ?>
		<?php echo $form->textField($model,'bookCode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bookCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exportedAt'); ?>
		<?php echo $form->textField($model,'exportedAt'); ?>
		<?php echo $form->error($model,'exportedAt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exportReason'); ?>
		<?php echo $form->textField($model,'exportReason',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'exportReason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exportedBy'); ?>
		<?php echo $form->textField($model,'exportedBy'); ?>
		<?php echo $form->error($model,'exportedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exportedByUser'); ?>
		<?php echo $form->textField($model,'exportedByUser',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'exportedByUser'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'totalAmount'); ?>
		<?php echo $form->textField($model,'totalAmount'); ?>
		<?php echo $form->error($model,'totalAmount'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->