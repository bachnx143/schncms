<?php
/* @var $this ExportbooksController */
/* @var $model Exportbooks */

$this->breadcrumbs=array(
	'Exportbooks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Exportbooks', 'url'=>array('index')),
	array('label'=>'Manage Exportbooks', 'url'=>array('admin')),
);
?>

<h1>Create Exportbooks</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>