<?php
/* @var $this ExportbooksController */
/* @var $model Exportbooks */

$this->breadcrumbs=array(
	'Exportbooks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Exportbooks', 'url'=>array('index')),
	array('label'=>'Create Exportbooks', 'url'=>array('create')),
	array('label'=>'View Exportbooks', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Exportbooks', 'url'=>array('admin')),
);
?>

<h1>Update Exportbooks <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>