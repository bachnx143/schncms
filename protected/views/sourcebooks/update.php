<?php
/* @var $this SourcebooksController */
/* @var $model Sourcebooks */

$this->breadcrumbs=array(
	'Sourcebooks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning" href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger" href="javascript:;"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $this->pageTitle; ?></h4>
            </div>
            <div class="panel-body" style="display: block;">
                <?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
    </div>
</div>

