<?php
/* @var $this SourcebooksController */
/* @var $model Sourcebooks */

$this->breadcrumbs=array(
	'Sourcebooks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Sourcebooks', 'url'=>array('index')),
	array('label'=>'Create Sourcebooks', 'url'=>array('create')),
	array('label'=>'Update Sourcebooks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Sourcebooks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sourcebooks', 'url'=>array('admin')),
);
?>

<h1>View Sourcebooks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sourceCode',
		'sourceName',
		'address',
		'phoneNumber',
		'emailAddress',
		'note',
	),
)); ?>
