<?php
/* @var $this SourcebooksController */
/* @var $model Sourcebooks */

$this->breadcrumbs=array(
	'Sourcebooks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Sourcebooks', 'url'=>array('index')),
	array('label'=>'Manage Sourcebooks', 'url'=>array('admin')),
);
?>

<h1>Create Sourcebooks</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>