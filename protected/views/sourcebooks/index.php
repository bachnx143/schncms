<?php
/* @var $this SourcebooksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sourcebooks',
);

$this->menu=array(
	array('label'=>'Create Sourcebooks', 'url'=>array('create')),
	array('label'=>'Manage Sourcebooks', 'url'=>array('admin')),
);
?>

<h1>Sourcebooks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
