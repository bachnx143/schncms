<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'books-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal'
    )
)); ?>

    <div class="col-md-6 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i
                            class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;"><i
                            class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger" href="javascript:;"
                       data-original-title="" title=""><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Input Types</h4>
            </div>
            <!-- End of heading Panel -->
            <div class="panel-body" style="display:block; ">
                <div id="add-books-alert" class="alert alert-warning fade in m-b-15" style="display: none;">
                    <span class="alt-content">
                        <strong>Warning!</strong>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </span>
                    <span data-dismiss="alert" class="close">×</span>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>

                    <div class="col-md-3">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Nhập sách mới' : 'Lưu thay đổi', array('class' => 'btn btn-sm btn-success btn-addbook')); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo CHtml::link('Huỷ! Quay lại', array('books/admin'), array('class' => 'btn btn-default'));?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'bookCode', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <?php echo $form->textField($model, 'bookCode', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control book-code', 'readonly' => 'readonly')); ?>
                        <?php echo $form->error($model, 'bookCode'); ?>
                    </div>
                    <!--<div class="col-md-3">
                        <?php //if ($model->isNewRecord): ?>
                            <?php //echo CHtml::link('Lấy mã mới', '#', array('class' => 'btn btn-primary renew-code')); ?>
                        <?php //endif; ?>
                    </div> -->
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'bookName', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php //echo $form->textField($model, 'bookName', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control')); ?>
                        <?php //echo $form->textField($model,'bookName',array('class' => 'form-control', 'placeholder' => 'Tên sách'));
                        $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                            // 'name'=>'ajaxrequest',
                            'model'=>$model,
                            'attribute' => 'bookName',
                            // additional javascript options for the autocomplete plugin
                            'options' => array(
                                'minLength' => '4',
                                'select' => "js:function(event, ui) {
                                        $.post('" . $this->createUrl("books/ajaxgetinfobook") . "',{bookname: ui.item.value}, function(returncode){
                                            //console.log(returncode);
                                            var obj = jQuery.parseJSON( returncode );

                                            if(obj.existbook){
                                                $('#Books_bookCode').val(obj.bookcode);
                                                $('#Books_importPrice').val(obj.importprice);
                                                $('#Books_exportPrice').val(obj.exportprice);
                                                $('#Books_units').val(obj.units);
                                                $('#Books_totalAmount').val(obj.totalamount);
                                                $('#Books_categoryId').val(obj.category);
                                                $('#Books_sourceId').val(obj.source);
                                                $('#Books_publishingYear').val(obj.publish);
                                                $('#Books_note').val(obj.note);
                                                $('.btn-addbook').val('Lưu thay đổi thông tin');
                                            }

                                            $('.bookQuantity').val('1');
                                        });

                                      }",
                            ),
                            'source'=>$this->createUrl("books/ajaxbookname"),
                            'htmlOptions'=>array(
                                'class' => 'form-control',
                                'placeholder' => 'Tên sách'
                            ),
                        ));
                        ?>
                        <?php echo $form->error($model, 'bookName'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'importPrice', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textField($model, 'importPrice', array('class' => 'form-control book-import-price')); ?>
                        <?php echo $form->error($model, 'importPrice'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'exportPrice', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textField($model, 'exportPrice', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control book-export-price')); ?>
                        <?php echo $form->error($model, 'exportPrice'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'categoryId', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <?php $lst_cate = CHtml::listData(Categories::model()->findAll(), "categoryCode", "categoryName"); ?>
                        <div class="cate-select">
                            <?php echo $form->dropDownList($model, 'categoryId', $lst_cate, array('class' => 'form-control selectpicker', 'empty' => 'Thể loại sách', 'data-live-search' => "true", 'data-style' => 'btn-white')); ?>
                        </div>
                        <?php echo $form->error($model, 'categoryId'); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo CHtml::link('Thêm thể loại', '#', array('class' => 'btn btn-default m-r-5 m-b-5', 'onclick' => '$("#add-categories").dialog("open"); return false;')); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end of panel 1 -->
    <!-- start panel 2 -->
    <div class="col-md-6 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i
                            class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;"><i
                            class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger" href="javascript:;"
                       data-original-title="" title=""><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Input Types</h4>
            </div>
            <div class="panel-body" style="display:block; ">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'sourceId', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <?php $lst_source = CHtml::listData(Sourcebooks::model()->findAll(), "sourceCode", "sourceName"); ?>
                        <div class="source-select">
                            <?php echo $form->dropDownList($model, 'sourceId', $lst_source, array('class' => 'form-controlselectpicker', 'empty' => 'Thể loại sách', 'data-live-search' => "true", 'data-style' => 'btn-white', 'empty' => ' Chọn nguồn ')); ?>
                        </div>
                        <?php echo $form->error($model, 'sourceId'); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo CHtml::link('Thêm nguồn', '#', array('class' => 'btn btn-default m-r-5 m-b-5', 'onclick' => '$("#add-sourcebook").dialog("open"); return false;')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'units', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textField($model, 'units', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Cuốn (Bộ)' ,'value' => 'cuốn')); ?>
                        <?php echo $form->error($model, 'units'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'quantity', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textField($model, 'quantity', array('class' => 'form-control books-quantity', 'value' => '1')); ?>
                        <?php echo $form->error($model, 'quantity'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'publishingYear', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php $listYear = array();
                        $i = date("Y", strtotime("1960"));
                        for ($i; $i <= date("Y"); $i++) {
                            $listYear[$i] = $i;
                        }
                        ?>
                        <?php echo $form->dropDownList($model, 'publishingYear', $listYear, array('class' => 'form-control', 'empty' => 'Chọn năm xuất bản')); ?>
                        <?php echo $form->error($model, 'publishingYear'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'totalAmount', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textField($model, 'totalAmount', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control book-total-amount')); ?>
                        <?php echo $form->error($model, 'totalAmount'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'note', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-9">
                        <?php echo $form->textArea($model, 'note', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'note'); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>
<?php
/** Start Add Source Books Widget **/
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'add-sourcebook',
    'options' => array(
        'title' => 'Thêm mới nguồn sách',
        'autoOpen' => false,
        'width' => 800,
    ),
));
$this->renderPartial('includes/_sourcebook_form', array('model' => $sourcemodel));
$this->endWidget('zii.widgets.jui.CJuiDialog');
/** End Add Source Books Widget **/
?>
<?php
/** Start Add Source Books Widget **/
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'add-categories',
    'options' => array(
        'title' => 'Thêm mới thể loại',
        'autoOpen' => false,
        'width' => 800,
    ),
));
$this->renderPartial('includes/_categories_form', array('model' => $categoriesmodel));
$this->endWidget('zii.widgets.jui.CJuiDialog');
/** End Add Source Books Widget **/
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#Books_categoryId').selectpicker();
        $('#Books_sourceId').selectpicker();
    });
</script>