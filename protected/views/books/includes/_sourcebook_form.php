<div class="row">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sourcebooks-bookform',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal'
        )
    )); ?>

    <div id="add-source-alert" class="alert alert-warning fade in m-b-15" style="display: none;">
        <span class="alt-content">
            <strong>Warning!</strong>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </span>
        <span data-dismiss="alert" class="close">×</span>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'sourceCode', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'sourceCode', array('class' => 'form-control source-code', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model, 'sourceCode'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'sourceName', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'sourceName', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'sourceName'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'address', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'address'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phoneNumber', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'phoneNumber', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'phoneNumber'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'emailAddress', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'emailAddress', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'emailAddress'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'note', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-9">
            <?php echo $form->textField($model, 'note', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'note'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label"></label>

        <div class="col-md-9">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Thêm mới nguồn' : 'Lưu thay đổi', array('class' => 'btn btn-success')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
Yii::app()->clientScript->registerScript('createSource', "
	var sourcecode_time = 'sources'+Math.round(+new Date()/1000);

	$('.source-code').val(sourcecode_time);

	/**  Validate source books */
	$('#sourcebooks-bookform').validate({
                onfocusout: true,
                onkeyup: true,
                onclick: true,
                focusInvalid: true,
                debug:true,
                rules:{
                    'Sourcebooks[sourceName]': 'required'
                },
                messages:{
                    'Sourcebooks[sourceName]': 'Chưa nhập tên nguồn sách. Vui lòng nhập. '
                },
                submitHandler: function(){
                    var formData = new FormData($('#sourcebooks-bookform')[0]);
                    var action = $('#sourcebooks-bookform').attr('action');
                    console.log('Validate Ok');
                    $.ajax({
                            url: action,
                            type: 'POST',
                            data: formData,
                            async: false,
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                console.log(obj.dropdown);
                                if(obj.status){
                                    $('.alt-content').html(obj.msg);
                                    $('#add-source-alert').removeAttr('style');
                                    $('#add-source-alert').removeClass('alert-warning');
                                    $('#add-source-alert').addClass('alert-success');
                                    $('.source-select').html(obj.dropdown);
                                }else{
                                    $('.alt-content').html(obj.msg);
                                    $('#add-source-alert').removeClass('alert-success');
                                    $('#add-source-alert').addClass('alert-warning');
                                    $('#add-source-alert').removeAttr('style');
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                }
            });
");
?>