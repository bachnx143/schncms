<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array(
        'class' => 'form-inline'
    )
)); ?>

	<div class="form-group">
		<?php //echo $form->textField($model,'bookCode',array('class' => 'form-control', 'placeholder' => 'Mã sách'));
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
               // 'name'=>'ajaxrequest',
                'model'=>$model,
                'attribute' => 'bookCode',
                // additional javascript options for the autocomplete plugin
                'options'=>array(
                    'minLength'=>'1',
                ),
                'source'=>$this->createUrl("books/ajaxbookcode"),
                'htmlOptions'=>array(
                    'class' => 'form-control',
                    'placeholder' => 'Mã sách'
                ),
            ));
        ?>
	</div>

	<div class="form-group">
		<?php //echo $form->textField($model,'bookName',array('class' => 'form-control', 'placeholder' => 'Tên sách'));
        $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
            // 'name'=>'ajaxrequest',
            'model'=>$model,
            'attribute' => 'bookName',
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'1',
            ),
            'source'=>$this->createUrl("books/ajaxbookname"),
            'htmlOptions'=>array(
                'class' => 'form-control',
                'placeholder' => 'Tên sách'
            ),
        ));
        ?>
	</div>

	<div class="form-group">
        <?php $listCate = CHtml::listData(Categories::model()->findAll(), "categoryCode", "categoryName");?>
		<?php echo $form->dropDownList($model,'categoryId', $listCate, array('class' => 'form-control search-cate-select selectpicker',
            'empty' => 'Thể loại', 'data-live-search' => "true", 'data-style' => 'btn-white')); ?>
	</div>

	<div class="form-group">
        <?php $listSource = CHtml::listData(Sourcebooks::model()->findAll(), "sourceCode", "sourceName");?>
		<?php echo $form->dropDownList($model,'sourceId', $listSource, array('class' => 'form-control search-source-select', 'empty' => 'Nguồn')); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Tìm sách', array('class' => 'btn btn-sm btn-primary m-r-5')); ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        //$('#Books_categoryId').selectpicker();
    });
</script>