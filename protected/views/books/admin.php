<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs = array(
    'Books' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#books-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('.search-cate-select').change(function(){
	$('.search-form form').submit();
});
");

$pageSize = Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']);
?>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i
                            class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;"
                       data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Nhập file</h4>
            </div>
            <div class="panel-body" style="display:none">
                <?php $link = Yii::app()->createUrl('books/importfile'); ?>
                <form class="form-inline" method="post" action="<?php echo $link; ?>" enctype='multipart/form-data'>
                    <div class="form-group">
                        <input type="file" name="import_file" value=""  class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="sheetname" value=""  class="form-control"/>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="isupdated" value="0"  class="form-control"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit_inport" class="btn btn-primary" >Tài file lên</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a data-click="panel-expand" class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;"><i
                            class="fa fa-expand"></i></a>
                    <a data-click="panel-reload" class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;"
                       data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                    <a data-click="panel-collapse" class="btn btn-xs btn-icon btn-circle btn-warning"
                       href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a>
                    <a data-click="panel-remove" class="btn btn-xs btn-icon btn-circle btn-danger"
                       href="javascript:;"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Quản lý sách</h4>
            </div>
            <div class="panel-body" style="display:block">
                <?php echo CHtml::link('Thêm sách mới', array('books/create'), array('class' => 'btn btn-primary m-r-5 m-b-5')); ?>
                <?php $this->renderPartial('_search', array(
                    'model' => $model,
                )); ?>
                <div class="table-responsive">
                    <?php
                    $form=$this->beginWidget('CActiveForm', array(
                        'enableAjaxValidation'=>true,
                    ));
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'books-grid',
                        'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'htmlOptions' => array(
                            'role' => 'grid'
                        ),
                        'columns' => array(
                            array(
                                'id'=>'autoId',
                                'class'=>'CCheckBoxColumn',
                                'selectableRows' => '50',
                            ),
                            array(
                                'name' => 'bookCode',
                                'type' => 'raw',
                                'headerHtmlOptions' => array(
                                    'class' => 'sorting'
                                )
                            ),
                            array(
                                'name' => 'bookName',
                                'type' => 'raw',
                                'headerHtmlOptions' => array(
                                    'class' => 'sorting'
                                )
                            ),
                            'importPrice',
                            'exportPrice',
                            array(
                                'name' => 'categoryId',
                                'type' => 'raw',
                                'value' => 'Functions::getCateName($data->categoryId)' //'$data->categoryId'
                            ),
                            'quantity',
                            'units',
                            /*
                            'sourceId',
                            'units',
                            'quantity',
                            'importedAt',
                            'importedBy',
                            'importedByUser',
                            'bookStatus',
                            'publishingYear',
                            'totalAmount',
                            'note',
                            'trash',
                            */
                            array(
                                'class' => 'CButtonColumn',
                                'template' => '{update}',
                                'header' => CHtml::dropDownList('pageSize', $pageSize, array(20 => 20, 50 => 50, 100 => 100), array(
                                        // change 'user-grid' to the actual id of your grid!!
                                        'onchange' => "$.fn.yiiGridView.update('books-grid',{ data:{pageSize: $(this).val() }})",
                                    )),
                            ),
                        ),
                    ));

                    echo CHtml::ajaxSubmitButton('Xoá sách',
                        array('books/ajaxupdate','act'=>'doDelete'),
                        array('success'=>'function(data){
                                obj = JSON.parse(data);
                                    alert(obj.msg);
                                    location.reload();
                                    //reloadGrid
                            }'),
                        array('class' => 'btn-ajax btn btn-warning',"onclick"=>"confirm('Bạn muốn xoá các cuốn sách được chọn?')"));

                    $this->endWidget();

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>