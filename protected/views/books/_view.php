<?php
/* @var $this BooksController */
/* @var $data Books */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookCode')); ?>:</b>
	<?php echo CHtml::encode($data->bookCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookName')); ?>:</b>
	<?php echo CHtml::encode($data->bookName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importPrice')); ?>:</b>
	<?php echo CHtml::encode($data->importPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exportPrice')); ?>:</b>
	<?php echo CHtml::encode($data->exportPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoryId')); ?>:</b>
	<?php echo CHtml::encode($data->categoryId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sourceId')); ?>:</b>
	<?php echo CHtml::encode($data->sourceId); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('units')); ?>:</b>
	<?php echo CHtml::encode($data->units); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importedAt')); ?>:</b>
	<?php echo CHtml::encode($data->importedAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importedBy')); ?>:</b>
	<?php echo CHtml::encode($data->importedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importedByUser')); ?>:</b>
	<?php echo CHtml::encode($data->importedByUser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookStatus')); ?>:</b>
	<?php echo CHtml::encode($data->bookStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publishingYear')); ?>:</b>
	<?php echo CHtml::encode($data->publishingYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalAmount')); ?>:</b>
	<?php echo CHtml::encode($data->totalAmount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trash')); ?>:</b>
	<?php echo CHtml::encode($data->trash); ?>
	<br />

	*/ ?>

</div>