<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs = array(
    'Books' => array('index'),
    'Create',
);
?>
<div class="row">
    <?php $this->renderPartial('_form', array('model' => $model, 'sourcemodel' => $sourcemodel, 'categoriesmodel' => $categoriesmodel)); ?>
</div>
<?php
$function = new Functions;
Yii::app()->clientScript->registerScript('createBooks', "
	var bookcode_time = '" . $function->generateTableCode("books") . "';

	$('.book-code').val(bookcode_time);

	$('.renew-code').click(function(){
	    var bookcoderenew_time = '" . $function->generateTableCode("books") . "';
	    $('.book-code').val(bookcoderenew_time);
	});

    $('.books-quantity').change(function(){
        var _bookImportprice = $('.book-import-price').val();
        var _bookQuantity = $(this).val();

        var _totalAmount = parseInt(_bookImportprice) * _bookQuantity;
        console.log(_bookImportprice * _bookQuantity);

        $('.book-total-amount').val(_totalAmount);
    });

    $('.book-import-price').change(function(){
        var _bookImportprice = $('.book-import-price').val();
        var _bookQuantity = $('.books-quantity').val();

        var _totalAmount = _bookImportprice * _bookQuantity;
        console.log(_bookImportprice * _bookQuantity);

        $('.book-total-amount').val(_totalAmount);
    });

	/**  Validate books form. */
	$('#books-form').validate({
                onfocusout: true,
                onkeyup: true,
                onclick: true,
                focusInvalid: true,
                debug:true,
                rules:{
                    'Books[bookName]': 'required',
                    'Books[importPrice]': 'required',
                    'Books[exportPrice]': 'required',
                    'Books[categoryId]': 'required',
                    'Books[sourceId]': 'required',
                    'Books[units]': 'required',
                    'Books[quantity]': 'required'
                },
                messages:{
                    'Books[bookName]': 'Vui lòng nhập tên sách đầy đủ.',
                    'Books[importPrice]': 'Chưa điền giá nhập sách. Vui lòng điền đầy đủ thông tin.',
                    'Books[exportPrice]': 'Chưa có giá bán sách. Vui lòng nhập giá. ',
                    'Books[categoryId]': 'Chưa có thông tin thể loại sách. Vui lòng chọn thể loại. ',
                    'Books[sourceId]': 'Chưa rõ nguồn nhập sách. Hãy chọn nguồn nhập. ',
                    'Books[units]': 'Chưa có đơn vị tính cho cuốn sách.',
                    'Books[quantity]': 'Số lượng sách là bao nhiêu. Vui lòng điền số lượng. '
                },
                submitHandler: function(){
                    var formData = new FormData($('#books-form')[0]);
                    var action = $('#books-form').attr('action');

                       $.ajax({
                            url: action,
                            type: 'POST',
                            data: formData,
                            async: false,
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                if(obj.status){
                                    $('.alt-content').html(obj.msg);
                                    $('#add-books-alert').removeAttr('style');
                                    $('#add-books-alert').removeClass('alert-warning');
                                    $('#add-books-alert').addClass('alert-success');
                                    location.reload();
                                }else{
                                    $('.alt-content').html(obj.msg);
                                    $('#add-books-alert').removeClass('alert-success');
                                    $('#add-books-alert').addClass('alert-warning');
                                    $('#add-books-alert').removeAttr('style');
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                }
            });
");
?>

