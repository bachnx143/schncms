<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.8/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jul 2015 10:01:40 GMT -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo Yii::app()->name; ?> | <?php echo $this->pageTitle; ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/css/style.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/css/style-responsive.min.css" rel="stylesheet"/>
    <link href="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/css/theme/default.css" rel="stylesheet" id="theme"/>
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<div class="login-cover">
    <div class="login-cover-image"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt=""/></div>
    <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <!-- begin brand -->
        <div class="login-header">
            <div class="brand">
                <!--<span class="logo"></span>-->
                <img class="logo-img" src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/schn-icon.png">
                 Sách Cũ Hà Nội
                <small>Hệ thống quản trị nội dung</small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'htmlOptions' => array(
                    'class' => 'margin-bottom-0'
                ),
            )); ?>

                <div class="form-group m-b-20">
                    <?php echo $form->textField($model,'username', array('class' => 'form-control input-lg', 'placeholder' => 'Tài khoản')); ?>
                    <?php echo $form->error($model,'username'); ?>
                </div>
                <div class="form-group m-b-20">
                    <!-- <input type="text" class="form-control input-lg" placeholder="Password"/> -->
                    <?php echo $form->passwordField($model,'password', array('class' => 'form-control input-lg', 'placeholder' => 'Mật khẩu')); ?>
                    <?php echo $form->error($model,'password'); ?>
                </div>
                <div class="checkbox m-b-20">
                    <!-- <label>
                        <input type="checkbox"/> Remember Me
                    </label> -->
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Đăng nhập</button>
                </div>
                <!-- <div class="m-t-20">
                    Not a member yet? Click <a href="#">here</a> to register.
                </div> -->
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <!-- end login -->

    <!-- <ul class="login-bg-list">
        <li class="active"><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-1.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-2.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-3.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-4.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-5.jpg" alt=""/></a></li>
        <li><a href="#" data-click="change-bg"><img src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/img/login-bg/bg-6.jpg" alt=""/></a></li>
    </ul> -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/crossbrowserjs/html5shiv.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/crossbrowserjs/respond.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/js/login-v2.demo.min.js"></script>
<script src="<?php echo $this->baseUrl; ?>/themes/schncms/views/layouts/assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
        LoginV2.init();
    });
</script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v1.8/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jul 2015 10:03:07 GMT -->
</html>
