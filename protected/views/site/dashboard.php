<!-- begin row -->
<div class="row">
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-green">
            <div class="stats-icon"><i class="fa fa-desktop"></i></div>
            <div class="stats-info">
                <h4>Số lượng đơn hàng hôm nay</h4>

                <p><?php echo Purchasebooks::model()->countOrder(); ?></p>
                <p class="summary">Offline/Online: <?php echo Purchasebooks::model()->countOrder("offline") ."/ ". Purchasebooks::model()->countOrder("online"); ?></p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-blue">
            <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
            <div class="stats-info">
                <h4>Tổng thu</h4>

                <p><?php
                        $totalAmount = Purchasebooks::model()->sumOrder();
                    echo number_format($totalAmount) . " VNĐ"; ?>
                </p>
                <p class="summary">Offline/Online:
                    <?php
                    $offlineAmount = Purchasebooks::model()->sumOrder("offline");
                    $onlineAmount = Purchasebooks::model()->sumOrder("online");
                    echo number_format($offlineAmount) ."/ ". number_format($onlineAmount) ." VNĐ"; ?></p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-purple">
            <div class="stats-icon"><i class="fa fa-users"></i></div>
            <div class="stats-info">
                <h4></h4>

                <p>0</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
            <div class="stats-info">
                <h4></h4>

                <p>0</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
</div>
<!-- end row -->
<!-- begin row -->
<div class="row">
<!-- begin col-8 -->
<div class="col-md-8">
<div class="panel panel-inverse" data-sortable-id="chart-js-2">
    <div class="panel-heading">
        <h4 class="panel-title">Số lượng đơn hàng (7 ngày gần nhất)</h4>
    </div>
    <div class="panel-body" id="container">

    </div>
</div>
</div>
<!-- end col-8 -->
<!-- begin col-4 -->
<div class="col-md-4">
    <div class="panel panel-inverse" data-sortable-id="index-6">
        <div class="panel-heading">
            <h4 class="panel-title">Danh sách</h4>
        </div>
        <div class="panel-body p-t-0">

            <table class="table table-valign-middle m-b-0">
                <thead>
                <tr>
                    <th>Ngày</th>
                    <th>Số đơn hàng</th>
                    <th>Tổng tiền</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $orderchartdate = $ordercharttotal = $orderchartamount = array();
                $totalOrder = $totalOrderAmount = 0;
                    foreach($dataorder as $item){
                        $totalOrder = $totalOrder + (int) $item->numberOfOrder;
                        $totalOrderAmount = $totalOrderAmount + (int) $item->amountOrder;

                        $orderchartdate[] = date("d", strtotime($item->orderAtDate));
                        $orderchartamount[] = (int) $item->amountOrder;
                        $ordercharttotal[] = (int) $item->numberOfOrder;
                        echo '<tr>';
                            echo '<td>'.date("d-m-Y", strtotime($item->orderAtDate)).'</td>';
                            echo '<td>'.$item->numberOfOrder.'</td>';
                            echo '<td>'.number_format($item->amountOrder).' VNĐ</td>';
                        echo '</tr>';
                    }
                ?>
                <tr style="font-weight: bold;">
                    <td>Tổng: </td>
                    <td><?php echo number_format($totalOrder); ?></td>
                    <td><?php echo number_format($totalOrderAmount) ." VNĐ"; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end col-4 -->
</div>
<!-- end row -->

<script>

    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Số lượng đơn hàng trong 7 ngày gần nhất'
            },
            subtitle: {
                text: 'Source: Sách Cũ Hà Nội'
            },
            xAxis: {
                categories: <?php echo json_encode($orderchartdate); ?>,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Số lượng (đơn hàng)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">Ngày {point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} đơn hàng </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Đơn hàng',
                data: <?php echo json_encode($ordercharttotal); ?>

            }]
        });
    });

</script>