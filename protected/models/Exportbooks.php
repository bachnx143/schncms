<?php

/**
 * This is the model class for table "{{exportbooks}}".
 *
 * The followings are the available columns in table '{{exportbooks}}':
 * @property integer $id
 * @property string $bookCode
 * @property integer $quantity
 * @property string $exportedAt
 * @property string $exportReason
 * @property integer $exportedBy
 * @property string $exportedByUser
 * @property integer $totalAmount
 */
class Exportbooks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{exportbooks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bookCode, quantity, exportReason', 'required'),
			array('quantity, exportedBy, totalAmount', 'numerical', 'integerOnly'=>true),
			array('bookCode', 'length', 'max'=>20),
			array('exportReason', 'length', 'max'=>120),
			array('exportedByUser', 'length', 'max'=>100),
			array('exportedAt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bookCode, quantity, exportedAt, exportReason, exportedBy, exportedByUser, totalAmount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bookCode' => 'Book Code',
			'quantity' => 'Quantity',
			'exportedAt' => 'Exported At',
			'exportReason' => 'Export Reason',
			'exportedBy' => 'Exported By',
			'exportedByUser' => 'Exported By User',
			'totalAmount' => 'Total Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bookCode',$this->bookCode,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('exportedAt',$this->exportedAt,true);
		$criteria->compare('exportReason',$this->exportReason,true);
		$criteria->compare('exportedBy',$this->exportedBy);
		$criteria->compare('exportedByUser',$this->exportedByUser,true);
		$criteria->compare('totalAmount',$this->totalAmount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Exportbooks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
