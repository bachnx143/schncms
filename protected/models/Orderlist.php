<?php

/**
 * This is the model class for table "{{orderlist}}".
 *
 * The followings are the available columns in table '{{orderlist}}':
 * @property integer $id
 * @property string $customerCode
 * @property string $bookCode
 * @property string $createdAt
 * @property string $note
 */
class Orderlist extends CActiveRecord
{
	public $totalQ;
    public $totalAmount;
    public $daterange;
    public $dstcatecode;
    public $categoryName;
    public $sourceName;
    public $dtsourcecode;
    public $bookName;
    public $importPrice;
    public $exportPrice;
    
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{orderlist}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, customerCode, bookCode', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('customerCode, bookCode', 'length', 'max'=>20),
            array('sourceCode, categoryCode', 'length', 'max'=>20),
			array('note', 'length', 'max'=>100),
			array('createdAt, purchaseCode', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, customerCode, bookCode, createdAt, note', 'safe', 'on'=>'search'),
            array('id, customerCode, bookCode, createdAt, note, categoryName, sourceName, daterange', 'safe', 'on'=>'getdetailbooks'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'books' => array(self::BELONGS_TO, 'Books', '', 'on'=> 'bookCode = books.bookCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customerCode' => 'Mã khách hàng',
			'bookCode' => 'Mã sách',
			'createdAt' => 'Ngày tạo',
            'purchaseCode' => 'Mã hoá đơn',
            'sourceCode' => 'Mã nguồn cung cấp',
            'categoryCode' => 'Mã loại',
			'note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customerCode',$this->customerCode,true);
		$criteria->compare('bookCode',$this->bookCode,true);
		$criteria->compare('createdAt',$this->createdAt,true);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orderlist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
     public function getdetailbooks(){
        
        $condition = "";
        $params = array();
        
        $today = date("Y-m-d");
        $beforesday = date("Y-m-d", strtotime("-7 days"));
        $condition .= "date(orlst.`createdAt`) BETWEEN :fromday AND :today";
        if(empty($this->daterange)){
            $params[":fromday"] = $beforesday;
            $params[":today"] = $today;
        }else{
            $dateexplode =  explode("-", $this->daterange);
            
            $fromdate = date("Y-m-d", strtotime($dateexplode[0]));
            $todate = date("Y-m-d", strtotime($dateexplode[1]));
            
            $params[":fromday"] = $fromdate;
            $params[":today"] = $todate;
        }
        
        
        if(!empty($this->sourceCode)){
            $condition .= " AND orlst.`sourceCode` = :srCode";
            $params[":srCode"] = $this->sourceCode;
        }
        
        $select = "";
        $select .= " books.bookCode ,books.bookName ,src.sourceName ,SUM(orlst.quantity) AS totalQ ,books.importPrice ,books.exportPrice ,(SUM(orlst.quantity) * books.exportPrice) AS totalAmount";
        $criteria = new CDbCriteria;
        $criteria->select = $select;
        $criteria->alias = "orlst";
        $criteria->join = "INNER JOIN `anc_books` books ON books.bookCode = orlst.bookCode INNER JOIN `anc_categories` cates ON cates.categoryCode = orlst.categoryCode INNER JOIN `anc_sourcebooks` src ON src.sourceCode = orlst.sourceCode";
        $criteria->group = "orlst.bookCode";
        $criteria->condition = $condition;
        $criteria->params = $params;
    
        return new CActiveDataProvider($this, array(
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
			'criteria'=>$criteria,
		));
        //return Orderlist::model()->findAll($criteria);
    }
    
    public function getFieldData(){
        $criteria = new CDbCriteria;
        $criteria->select = "DISTINCT(orlst.`categoryCode`) AS dstcatecode, cates.`categoryName` AS categoryName";
        $criteria->alias = "orlst";
        $criteria->join = "INNER JOIN `anc_categories` cates ON cates.`categoryCode` = orlst.`categoryCode`";
        
        $findst = $this->model()->findAll($criteria);
        $lstArray = array();
        foreach($findst as $item){
            $lstArray[$item->dstcatecode] = $item->categoryName;
        }
        
        return $lstArray;
    }    
    
    public function getSourceData(){
        $criteria = new CDbCriteria;
        $criteria->select = "DISTINCT(orlst.`sourceCode`) AS dtsourcecode, src.`sourceName` AS sourceName";
        $criteria->alias = "orlst";
        $criteria->join = "INNER JOIN `anc_sourcebooks` src ON src.`sourceCode` = orlst.`sourceCode`";
        
        $findst = $this->model()->findAll($criteria);
        $lstSource = array();
        foreach($findst as $item){
            $lstSource[$item->dtsourcecode] = $item->sourceName;
        }
        
        return $lstSource;
    }    
}
