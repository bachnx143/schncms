<?php

/**
 * This is the model class for table "{{manageaccounts}}".
 *
 * The followings are the available columns in table '{{manageaccounts}}':
 * @property integer $id
 * @property string $accountCode
 * @property string $accountLogin
 * @property string $accountSecret
 * @property string $accountName
 * @property string $createdAt
 * @property string $roleCode
 * @property string $accountEmail
 * @property string $phoneNumber
 * @property integer $activeStatus
 * @property string $accountSecretKey
 * @property string $lastLoginAt
 * @property string $lastLoginIn
 * @property integer $onLogin
 */
class Manageaccounts extends CActiveRecord
{

    public $roleName;
    public $accountReSecret;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{manageaccounts}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('accountCode, accountLogin, accountSecret, roleCode', 'required'),
            array('activeStatus, onLogin', 'numerical', 'integerOnly' => true),
            array('accountCode, roleCode, lastLoginIn', 'length', 'max' => 20),
            array('accountLogin, accountSecret, accountEmail', 'length', 'max' => 100),
            array('accountName', 'length', 'max' => 120),
            array('phoneNumber', 'length', 'max' => 15),
            array('accountSecretKey', 'length', 'max' => 10),
            array('createdAt, lastLoginAt', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, accountCode, accountLogin, accountSecret, accountName, createdAt, roleCode, accountEmail, phoneNumber, activeStatus, accountSecretKey, lastLoginAt, lastLoginIn, onLogin', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'roles' => array(self::HAS_ONE, 'Roles', '', 'on' => 'roleCode = roles.roleCode'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'accountCode' => 'Mã tài khoản',
            'accountLogin' => 'Tài khoản',
            'accountSecret' => 'Mật khẩu',
            'accountReSecret' => 'Nhập lại mật khẩu',
            'accountName' => 'Tên đầy đủ',
            'createdAt' => 'Ngày tạo',
            'roleCode' => 'Quyền truy cập',
            'accountEmail' => 'Địa chỉ E-mail',
            'phoneNumber' => 'Số điện thoại',
            'activeStatus' => 'Trạng thái',
            'accountSecretKey' => 'Account Secret Key',
            'lastLoginAt' => 'Lần cuối đăng nhập lúc',
            'lastLoginIn' => 'Lần cuối đăng nhập tại',
            'onLogin' => 'Trạng thái đăng nhập',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('accountCode', $this->accountCode, true);
        $criteria->compare('accountLogin', $this->accountLogin, true);
        $criteria->compare('accountSecret', $this->accountSecret, true);
        $criteria->compare('accountName', $this->accountName, true);
        $criteria->compare('createdAt', $this->createdAt, true);
        $criteria->compare('roleCode', $this->roleCode, true);
        $criteria->compare('accountEmail', $this->accountEmail, true);
        $criteria->compare('phoneNumber', $this->phoneNumber, true);
        $criteria->compare('activeStatus', $this->activeStatus);
        $criteria->compare('accountSecretKey', $this->accountSecretKey, true);
        $criteria->compare('lastLoginAt', $this->lastLoginAt, true);
        $criteria->compare('lastLoginIn', $this->lastLoginIn, true);
        $criteria->compare('onLogin', $this->onLogin);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function validatePassword($password, $secretKey)
    {
        $function = new Functions;
        return $function->hashPassword($password, $secretKey) == $this->accountSecret;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Manageaccounts the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAccountInfo($accountName, $fetch = false, $fetchName = "")
    {
        $model = new CDbCriteria;
        $model->select = "t.*, rl.roleName as roleName";
        $model->condition = "accountLogin = :acclogin";
        $model->params = array(":acclogin" => $accountName);
        $model->join = "INNER JOIN anc_roles rl ON rl.roleCode = t.roleCode";

        $findData = $this->model()->find($model);
        if(!empty($findData)){
            if ($fetch)
                return $findData->$fetchName;
            else
                return $findData;
        }else{
            return "";
        }

    }
}
