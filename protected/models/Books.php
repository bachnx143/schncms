<?php

/**
 * This is the model class for table "{{books}}".
 *
 * The followings are the available columns in table '{{books}}':
 * @property integer $id
 * @property string $bookCode
 * @property string $bookName
 * @property string $importPrice
 * @property string $exportPrice
 * @property integer $categoryId
 * @property integer $sourceId
 * @property string $units
 * @property integer $quantity
 * @property string $importedAt
 * @property integer $importedBy
 * @property string $importedByUser
 * @property integer $bookStatus
 * @property string $publishingYear
 * @property string $totalAmount
 * @property string $note
 * @property integer $trash
 */
class Books extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{books}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quantity, importedBy, bookStatus, trash', 'numerical', 'integerOnly'=>true),
			array('bookCode, categoryId, sourceId, updatedBy', 'length', 'max'=>22),
			array('bookName, importedByUser', 'length', 'max'=>150),
			array('importPrice, exportPrice, units', 'length', 'max'=>50),
			array('totalAmount', 'length', 'max'=>100),
			array('note', 'length', 'max'=>45),
			array('importedAt, publishingYear, updatedAt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bookCode, bookName, importPrice, exportPrice, categoryId, sourceId, units, quantity, importedAt, importedBy, importedByUser, bookStatus, publishingYear, totalAmount, note, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'cate' => array(self::BELONGS_TO, 'Categories', '', 'on' => 't.categoryId = cate.categoryCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bookCode' => 'Mã sách',
			'bookName' => 'Tên sách',
			'importPrice' => 'Giá nhập',
			'exportPrice' => 'Giá bán',
			'categoryId' => 'Thể loại',
			'sourceId' => 'Nguồn',
			'units' => 'Đơn vị',
			'quantity' => 'Số lượng',
			'importedAt' => 'Ngày nhập',
			'importedBy' => 'Mã người nhập',
			'importedByUser' => 'Người nhập',
            'updatedBy' => 'Được cập nhật bởi',
            'updatedAt' => 'Cập nhật vào lúc',
			'bookStatus' => 'Trạng thái sách',
			'publishingYear' => 'Năm xuất bản',
			'totalAmount' => 'Tổng tiền',
			'note' => 'Ghi chú',
			'trash' => 'Thùng rác',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bookCode',$this->bookCode,true);
		$criteria->compare('bookName',trim($this->bookName),true);
		$criteria->compare('importPrice',$this->importPrice,true);
		$criteria->compare('exportPrice',$this->exportPrice,true);
		$criteria->compare('categoryId',$this->categoryId);
		$criteria->compare('sourceId',$this->sourceId);
		$criteria->compare('units',$this->units,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('importedAt',$this->importedAt,true);
		$criteria->compare('importedBy',$this->importedBy);
		$criteria->compare('importedByUser',$this->importedByUser,true);
		$criteria->compare('bookStatus',$this->bookStatus);
		$criteria->compare('publishingYear',$this->publishingYear,true);
		$criteria->compare('totalAmount',$this->totalAmount,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('trash',$this->trash);

        return new CActiveDataProvider(get_class($this),array(
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
            'criteria'=>$criteria,
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Books the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
