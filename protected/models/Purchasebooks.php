<?php

/**
 * This is the model class for table "{{purchasebooks}}".
 *
 * The followings are the available columns in table '{{purchasebooks}}':
 * @property integer $id
 * @property string $orderCode
 * @property string $orderAt
 * @property integer $customerId
 * @property string $customerCode
 * @property integer $paymentMethod
 * @property integer $transportMethod
 * @property integer $orderStatus
 * @property string $shippingAddess
 * @property string $note
 */
class Purchasebooks extends CActiveRecord
{

    public $sumOrder = 0;
    public $orderAtDate;
    public $numberOfOrder;
    public $amountOrder;
    public $fromorder;
    public $toorder;
    public $ordername;
    public $orderPhone;
    public $orderEmail;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchasebooks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customerId, paymentMethod, transportMethod, orderStatus, quantity, totalAmount, orderType', 'numerical', 'integerOnly'=>true),
			array('orderCode, customerCode, purchaseCode', 'length', 'max'=>22),
			array('shippingAddess', 'length', 'max'=>250),
            array('shippingFee', 'length', 'max'=>50),
			array('note', 'length', 'max'=>100),
			array('orderAt, quantity, totalAmount, purchaseCode', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, orderCode, orderAt, customerId, customerCode, paymentMethod, transportMethod, orderStatus, shippingAddess, note, ordername, orderPhone, orderEmail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'orderCode' => 'Đơn hàng số',
			'orderAt' => 'Ngày tạo',
			'customerId' => 'Customer',
			'customerCode' => 'Mã khách hàng',
			'paymentMethod' => 'Hình thức thanh toán',
			'transportMethod' => 'Hình thức vận chuyển',
			'orderStatus' => 'Trạng thái đơn hàng',
			'shippingAddess' => 'Địa chỉ nhận hàng',
            'totalAmount' => 'Tổng tiền thanh toán',
            'orderType' => 'Loại đơn hàng',
            'shippingFee' => 'Phí vận chuyển',
            'ordername' => 'Tên khách hàng',
            'orderPhone' => 'Số điện thoại',
            'orderEmail' => 'E-mail khách hàng',
			'note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
        $condition = "";
        $params = array();
        $findCusCondition = "";
        $findCusParams = array();
        
		$criteria=new CDbCriteria;
        if(!empty($this->ordername)){
            $findCusCondition = "customerName = :name";
            $findCusParams[":name"] = $this->ordername;
        }
        
        if(!empty($this->orderPhone)){
            $findCusCondition = "phoneNumber = :phone";
            $findCusParams[":phone"] = $this->orderPhone;
        }
        
        if(!empty($this->orderEmail)){
            $findCusCondition = "emailAddress = :email";
            $findCusParams[":email"] = $this->orderEmail;
        }
            
        if(!empty($findCusCondition)){
            $findCus = Customers::model()->find($findCusCondition, $findCusParams);
            if(!empty($findCus))
                $this->customerCode = $findCus->customerCode;
        }
        
        $criteria->order = "orderAt desc";

		$criteria->compare('id',$this->id);
		$criteria->compare('orderCode',$this->orderCode,true);
		$criteria->compare('orderAt',$this->orderAt,true);
		$criteria->compare('customerId',$this->customerId);
		$criteria->compare('customerCode',$this->customerCode,true);
		$criteria->compare('paymentMethod',$this->paymentMethod);
		$criteria->compare('transportMethod',$this->transportMethod);
		$criteria->compare('orderStatus',$this->orderStatus);
		$criteria->compare('shippingAddess',$this->shippingAddess,true);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function countOrder($orderType=""){
        // @todo Please modify the following code to remove attributes that should not be searched.

        $condition = "";
        $params = array();
        $today = date("Y-m-d");
        $type = 0;

        $criteria = new CDbCriteria;
        $condition .= "date(orderAt) between :fromday and :today";

        $params[":fromday"] = $today;
        $params[":today"] = $today;

        if(!empty($orderType)){
            switch($orderType){
                case "online":
                    $type = 1;
                    break;
                case "offline":
                    $type = 0;
                    break;
            }

            $condition .= " and orderType = :ordertype";
            $params[":ordertype"] = $type;
        }

        $criteria->condition = $condition;
        $criteria->params = $params;

        return $this->model()->count($criteria);
    }

    public function sumOrder($orderType = ""){
        $condition = "";
        $params = array();
        $today = date("Y-m-d");
        $type = 0;

        $criteria = new CDbCriteria;
        $condition .= "date(orderAt) between :fromday and :today";

        $params[":fromday"] = $today;
        $params[":today"] = $today;

        if(!empty($orderType)){
            switch($orderType){
                case "online":
                    $type = 1;
                    break;
                case "offline":
                    $type = 0;
                    break;
            }

            $condition .= " and orderType = :ordertype";
            $params[":ordertype"] = $type;
        }
        $criteria->select = "SUM(totalAmount) as sumOrder";
        $criteria->condition = $condition;
        $criteria->params = $params;
        $model = $this->model()->find($criteria);
        return $model->sumOrder;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Purchasebooks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getLastSeven(){
        $today = date("Y-m-d");
        $beforeSday = date("Y-m-d", strtotime("-7 days"));

        $creteria = new CDbCriteria;
        $creteria->select = "date(orderAt) AS orderAtDate, COUNT(*) AS numberOfOrder, SUM(totalAmount) AS amountOrder";
        $creteria->condition = "date(orderAt) BETWEEN :fromorder AND :toorder";
        $creteria->params = array(":fromorder" => $beforeSday, ":toorder" => $today);
        $creteria->order = "date(orderAt) ASC";
        $creteria->group = "date(orderAt)";

        return $this->model()->findAll($creteria);
    }
}

