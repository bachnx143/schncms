<?php

class Statistics extends CActiveRecord
{
    public $bookName;
    public $totalQuantity;
    public $importPrice;
    public $exportPrice;
    public $finalAmount;
    public $sourceCode;
    public $categoryName;
    public $sourceName;
    public $orderAt;
    public $fromOrder;
    public $toOrder;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Orderlist the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function booksStatisticDetail(){
        //SELECT books.bookName AS bookName, COUNT(orlst.`quantity`) AS totalQuantity, books.importPrice AS importPrice,
        //books.exportPrice AS exportPrice, (books.exportPrice * COUNT(orlst.`quantity`)) AS `finalAmount`,
        //src.sourceCode AS sourceCode, src.sourceName AS sourceName, date(orlst.`createdAt`) AS orderAt
        //FROM `anc_orderlist` orlst
        //INNER JOIN `anc_books` books ON books.bookCode = orlst.bookCode
        //INNER JOIN `anc_sourcebooks` src ON src.sourceCode = books.sourceId
        //WHERE src.sourceCode = 'NS20150810172525037' AND date(orlst.`createdAt`) BETWEEN '2015-08-11' AND '2015-08-16'
        //GROUP BY orlst.bookCode ORDER BY date(orlst.`createdAt`) ASC

        $select = "";
        $select .= "books.bookName AS bookName, COUNT(orlst.`quantity`) AS totalQuantity, books.importPrice AS importPrice";
        $select .= ", books.exportPrice AS exportPrice, (books.exportPrice * COUNT(orlst.`quantity`)) AS `finalAmount`";
        $select .= ", src.sourceCode AS sourceCode, src.sourceName AS sourceName, date(orlst.`createdAt`) AS orderAt, cate.categoryName AS categoryName ";

        $today = date("Y-m-d");
        if(empty($this->fromOrder) && empty($this->toOrder))
            $this->fromOrder = $this->toOrder = $today;

        $condition = "";
        $params = array();

        $criteria = new CDbCriteria;
        $criteria->select = $select;
        $criteria->alias = "orlst";
        $criteria->join = "INNER JOIN `anc_books` books ON books.bookCode = orlst.bookCode ";
        $criteria->join = "INNER JOIN `anc_sourcebooks` src ON src.sourceCode = books.sourceId";
        $criteria->join = "INNER JOIN `anc_categories` cate ON cate.categoryCode = books.categoryId";

        $condition = "date(orlst.`createdAt`) BETWEEB :from AND :to";
        $params[":from"] = $this->fromOrder;
        $params[":to"] = $this->toOrder;


        $criteria->compare('sourceCode',$this->sourceCode,true);
        $criteria->compare('cateoryCode',$this->cateoryCode,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));

    }
        
   


}