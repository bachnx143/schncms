<?php

class Functions extends Controller{

    private  $basePath;
    private  $cacheExpiredTime = 600;

    public function __construct(){
        $this->basePath = dirname(Yii::app()->request->scriptFile);
        $this->cacheExpiredTime = 600;
    }

    /**
     * @function hashPassword()
     * Return hash code for password
     */
    public function hashPassword($password,$salt){
        $passwordarr = unpack("C*",md5($password));//convert thành m?ng byte t? mã MD5 string c?a password
        $saltarr = unpack("C*",$salt);;//convert thành m?ng byte t? salt

        $salt_len = count($saltarr);
        $pass_len = count($passwordarr);

        $binarr_raw = null;
        if($salt_len>0){
            for($i=0; $i<$pass_len; $i++){
                //Mix with XOR
                $index = $i % $salt_len;
                $binarr_raw .= chr($passwordarr[$i+1] ^ $saltarr[$index+1]);//n?i vào chu?i d?ng binary raw
            }
        }
        else{
            return md5($password);
        }
        //$base64_str = base64_encode($binarr_raw);//l?y text base64 t? chu?i d?ng binary raw
        //base64 removed for java support (not need add library)
        $final_hash = md5($binarr_raw);//mã hóa md5 binary
        return $final_hash;
    }/** End Of hashPassword */

    /**
     *
     * @function: generatesalt($length = 5);
     * @description: auto generation a salt string.
     */

    function generatesalt($length = 5){
        $str_accept = '0123456789abcdefghijklmnopqrstuwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $strlen = strlen($str_accept);
        $rand_string = '';
        for($i=0;$i<$length;$i++){
            $index = rand()%$strlen;//fix
            $rand_string .= $str_accept[$index];
        }
        return $rand_string;
    }


    /**
     * @function readFile()
     * @params (array) data
     */
    public function readFile($data){
        $data = (object) $data;
        $filefata = file_get_contents($this->basePath.'/config/'.$data->filename);

        return $filefata;
    }/** End Of readFile */

    /**
     * @function writeFile()
     *
     * @access public
     * @param (array) data
     */
    public function writeFile($data){
        $objectdata = (object) $data;
        $pathfile = $this->basePath.'/config/'.$objectdata->filename;

        $file = fopen($pathfile,'w+');
        fwrite($file,json_encode($objectdata->filedata));
        fclose($file);
    } /** End Of writeFile */

    /**
     * @function: generateTableCode
     *
    */
    public function generateTableCode($strTable){
        $strHead = "";
        $datecode =  $this->autoNextNumber();

        switch($strTable){
            case "account":
                    $strHead="TK"; // Tài Khoản
                break;
            case "customer":
                    $strHead="KH";
                break;
            case "roles":
                    $strHead="TC"; // Truy Cập
                break;
            case "books":
                    $strHead="SC"; // Sách Cũ
                break;
            case "categories":
                    $strHead="TL"; // Thể Loại
                break;
            case "sourcebook":
                    $strHead="NS"; // Nguồn Sách
                break;
            case "purchase":
                    $strHead="DH";
                break;
        }

        return $strHead.$datecode;
    }

    /**
     * @function autoNextNumber
     * @params:
     */
    public function autoNextNumber(){
        $autonext = 0;
        $incrimentnumber = "";
        $autonext = $this->readFile(array('filename' => 'autoincriment.txt'));

        if($autonext > 1000)
            $autonext = 0;
        else
            $autonext = $autonext + 1;

        $this->writeFile(array('filename' => 'autoincriment.txt', 'filedata' => $autonext));
        $now = date("YmdHis");
        if($autonext < 10)
            $incrimentnumber = "00".$autonext;
        elseif($autonext >= 10 && $autonext < 100)
            $incrimentnumber = "0".$autonext;
        elseif($autonext >= 100 && $autonext < 1000)
            $incrimentnumber = $autonext;
        else
            $incrimentnumber = $autonext;

        return $now.$incrimentnumber;
    }

    public static function getCateName($cateCode){
        $findModel = Categories::model()->find("categoryCode = :cateCode", array(':cateCode' => $cateCode));
        if(!empty($findModel))
            return $findModel->categoryName;

        return "";
    }

    /**
     * @function diacriticalMarks
     * Convert Vietnamese to slug.
     */
    public function diacriticalMarks($str)
    {
        $finalString="";

        $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
            "è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
            "ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ"," ");

        $marKoDau=array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","-");
        $str=str_replace($marTViet,$marKoDau,$str);

        $dau = array(',','"',':',"(",")","?", "'","’","‘",",");
        $content = str_replace($dau, "", $str);
        $editdau  = array("/","&","!");
        $rcontent = str_replace($editdau,"-",$content);

        $finalString = strtolower($rcontent);

        return $finalString;

    } /** End Of diacriticalMarks */

    public static function getBookInfo($bookCode, $fieldName){
        $model = Books::model()->find("bookCode = :bookcode", array(":bookcode" => $bookCode));
        return $model->$fieldName;
    }
}