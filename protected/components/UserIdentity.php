<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_USERNAME_INACTIVE=67;
    const ERROR_VERIFYCODE_INVALID = 66;
    private $_id;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $username = $this->username;

        $users = Manageaccounts::model()->find('accountLogin = :uname', array(':uname' => $username));

        if($users === null){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }else if(!$users->validatePassword($this->password, $users->accountSecretKey)){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }else{
            $this->errorCode = self::ERROR_NONE;
            Yii::app()->session['loggedin'] = true;
            $findUser = Manageaccounts::model()->getAccountInfo($username);
            $this->setState('roles', $findUser->roleName);
            $this->setState('usercode', $users->accountCode);
          //  $this->setState('referrer', $users->referer);
          //  $this->setState('id', $users->id);
            $this->setState('fullname', $users->accountName);
            $this->_id = $users->id;
        }

        return !$this->errorCode;
    }

    public function getId(){
        return $this->_id;
    }
}